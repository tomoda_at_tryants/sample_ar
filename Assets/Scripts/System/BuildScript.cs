﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;


public class BuildScript : MonoBehaviour 
{
	public const string		s_ScenePath = "Assets/Scenes/";

	//	ビルドシーン
	private static string[]	Scenes_AuSmartPass = new string[3]
	{
		s_ScenePath + "s_auth.unity",
		s_ScenePath + "scn_title.unity",
		s_ScenePath + "scn_game.unity"
	};
	private static string[] Scenes_GooglePlay = new string[2]
	{
		s_ScenePath + "scn_title.unity",
		s_ScenePath + "scn_game.unity"
	};

	//	パッケージ名
	private static string s_BundleIdentifier_AuStartPass	= "untei.au";
	private static string s_BundleIdentifier_GooglePlay		= "net.bonusstage.unteidx";

	static string	a_Path = "E:/User/Tomoda/Desktop/";
	//	apk名
	private static string s_ApkName_AuStartPass = a_Path + "untei_au.apk";
	private static string s_ApkName_GooglePlay	= a_Path + "untei_googleplay.apk"; 

	//	バージョン
	private static string s_BundleVersion_AuSmartPass	= "4.1";
	private static string s_BundleVersion_GooglePlay	= "2.1";

	//	バージョンコード
	private static int	s_BundleVersionCode_AuStartPass	= 6;
	private static int	s_BundleVersionCode_GooglePlay	= 3;

	//	キーストア名
	private static string	s_KeyStoreName	= "E:/User/Tomoda/Desktop/UnityProject/untei/untei_dx/Assets/untei.keystore";

	//	キーストアパスワード
	private static string s_KeyStorePass	= "up5tart";

	//	キーアライアス名
	private static string	s_KeyAliasName	= "untei";

	//	キーアライアスパスワード
	private static string	s_KeyAliasPass	= "up5tart";

	/// <summary>
	/// auスマートパス版のビルドを行う
	/// </summary>
	[MenuItem("BuildScript/au SmartPass/Build", false)]
	public static void auSmartPass_Build()
	{
		//	ビルド中なら終了
		if (BuildPipeline.isBuildingPlayer) return;
		Build(Scenes_AuSmartPass, s_ApkName_AuStartPass, s_BundleIdentifier_AuStartPass, s_BundleVersion_AuSmartPass, s_BundleVersionCode_AuStartPass, BuildTarget.Android, BuildOptions.None);
	}

	/// <summary>
	/// auスマートパス版のビルドとランを行う
	/// </summary>
	[MenuItem("BuildScript/au SmartPass/Build And Run", false)]
	public static void auSmartPass_BuildAndRun()
	{
		//	ビルド中なら終了
		if (BuildPipeline.isBuildingPlayer) return;

		Build(Scenes_AuSmartPass, s_ApkName_AuStartPass, s_BundleIdentifier_AuStartPass, s_BundleVersion_AuSmartPass, s_BundleVersionCode_AuStartPass, BuildTarget.Android, BuildOptions.AutoRunPlayer);
	}

	/// <summary>
	/// GooglePlay版のビルドを行う
	/// </summary>
	[MenuItem("BuildScript/GooglePlay/Build", false)]
	public static void GooglePlay_Build()
	{
		//	ビルド中なら終了
		if (BuildPipeline.isBuildingPlayer) return;

		Build(Scenes_GooglePlay, s_ApkName_GooglePlay, s_BundleIdentifier_GooglePlay, s_BundleVersion_GooglePlay, s_BundleVersionCode_GooglePlay, BuildTarget.Android, BuildOptions.None);
	}

	/// <summary>
	/// GooglePlay版のビルドとランを行う
	/// </summary>
	[MenuItem("BuildScript/GooglePlay/Build And Run", false)]
	public static void GooglePlay_BuildAndRun()
	{
		//	ビルド中なら終了
		if (BuildPipeline.isBuildingPlayer) return;
		//	ビルド
		Build(Scenes_GooglePlay, s_ApkName_GooglePlay, s_BundleIdentifier_GooglePlay, s_BundleVersion_GooglePlay, s_BundleVersionCode_GooglePlay, BuildTarget.Android, BuildOptions.AutoRunPlayer);
	}

	/// <summary>
	/// ビルド処理
	/// </summary>
	/// <param name="p_BundleIdentifier"></param>
	/// <param name="p_BundleVersion"></param>
	/// <param name="p_nBundleVersionCode"></param>
	/// <param name="p_BuildTarget"></param>
	/// <param name="p_BuildOptions"></param>
	private static void	Build(string[] p_Scenes, string p_ApkName, string p_BundleIdentifier, string p_BundleVersion, int p_nBundleVersionCode, BuildTarget p_BuildTarget, BuildOptions p_BuildOptions)
	{
		//	ビルド中なら終了
		if (BuildPipeline.isBuildingPlayer) return;

		//	パッケージ名
		PlayerSettings.bundleIdentifier				= p_BundleIdentifier;
		//	バージョン
		PlayerSettings.bundleVersion				= p_BundleVersion;
		//	バージョンコード
		PlayerSettings.Android.bundleVersionCode	= p_nBundleVersionCode;

		//	KeyStore名
		PlayerSettings.Android.keystoreName = s_KeyStoreName;
		//	KeyStoreパスワード
		PlayerSettings.Android.keystorePass = s_KeyStorePass;
		//	KeyAlias名
		PlayerSettings.Android.keyaliasName = s_KeyAliasName;
		//	KeyAliasパスワード
		PlayerSettings.Android.keyaliasPass	= s_KeyAliasPass;

		//	ビルド
		BuildPipeline.BuildPlayer(p_Scenes, p_ApkName, p_BuildTarget, p_BuildOptions);
	}
}

#endif