﻿#if RELEASE
	#if ENABLE_DEBUG
		#define		_ENABLE_DEBUG_COMMAND
	#endif
#else
	#if FOR_SHOOTING
		// 撮影用の時はデバッグコマンド無効
	#else
		#define		_ENABLE_DEBUG_COMMAND
	#endif
#endif

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.SceneManagement; 

public class TADebug : MonoBehaviour {

	static float c_Height = 640.0f;
	static float c_Width = 1136.0f;

	public delegate void guiDrawDelegate(int p_nWinID);
	
	static TADebug s_Instance;
	public static TADebug instance
	{
		get 
		{
			if (s_Instance == null)
			{
				Create();
			}

			return s_Instance; 
		}
	}

	static guiDrawDelegate s_GuiDrawDelgate;
	
	static string s_Log;
	static bool s_bVisible;
	public static bool visible
	{
		get { return s_bVisible; }
		set { s_bVisible = value; }
	}

	static bool s_bLogging;
	public static bool logging
	{
		get { return s_bLogging; }
		set { s_bLogging = value; }
	}

	static bool s_bStdOutput;
	public static bool stdOutput
	{
		get { return s_bStdOutput; }
		set { s_bStdOutput = value; }
	}

	public static GUISkin skin;

	public static TADebug Create()
	{
		if (s_Instance != null)
		{
			return s_Instance;
		}
		
		GameObject obj = new GameObject("TADebug");
		s_Instance = obj.AddComponent<TADebug>();

		return s_Instance;
	}
	
	void Awake()
	{
#if false	// TODO
		if (!Debug.isDebugBuild)
		{
			Destroy(gameObject);
			return;
		}
#endif
		if ((s_Instance != null) && (s_Instance != this))
		{
			Destroy(gameObject);
			return;
		}

//		GUIDrawDelegateAppend("●", OnGuiDrawItemWindow, 0);
		GUIDrawDelegateAppend("BTN", OnGuiDrawButtonWindow, 0);
		GUIDrawDelegateAppend("LOG", onGuiDrawLogWindow, 0);
		GUIDrawDelegateAppend("SCN", onGuiDrawScnWindow, 0);
		
		DontDestroyOnLoad(gameObject);
		s_Instance = this;

		if (skin == null)
		{
			try
			{
				skin = (GUISkin)Resources.Load(defSystem.c_PrefabPath + "TADebugGUISkin", typeof(GUISkin));
			}
			catch
			{
				skin = null;
			}
		}

		LogClear();

#if UNITY_EDITOR
	#if RELEASE
		// リリース時
		#if _ENABLE_DEBUG_COMMAND
			// デバッグコマンド有効時
			s_bVisible = false;
			s_bLogging = false;
			s_bStdOutput = true;
		#else
			// デバッグコマンド無効時（本番）
			s_bVisible = false;
			s_bLogging = false;
			s_bStdOutput = false;
		#endif
	#else
		// デバッグ時
		s_bVisible = false;
		s_bLogging = true;
		s_bStdOutput = true;
	#endif
#else
	// 実機時
	#if _ENABLE_DEBUG_COMMAND
		// デバッグコマンド無効時（本番）
		s_bVisible = false;
		s_bLogging = false;
		s_bStdOutput = false;
	#else
		// デバッグコマンド無効時（本番）
		s_bVisible = false;
		s_bLogging = false;
		s_bStdOutput = false;
	#endif
#endif
	}
	
	/// <summary>
	/// static変数の初期化
	/// （リブート時に呼ばれる)
	/// </summary>
	public static void ColdInit()
	{
		if (s_Instance != null) DestroyImmediate(s_Instance.gameObject);
		s_Instance = null;

		s_GuiDrawDelgate = null;
		s_Log = null;
		s_bVisible = false;
		s_bLogging = false;
		s_bStdOutput = false;
		skin = null;

		s_ToolbarInfo.Clear();
		s_ToolbarInfo0.Clear();
	}

	void OnApplicationQuit()
	{
		Destroy(gameObject);
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public static void	LogClear()
	{
		s_Log = "";
	}
	
	public static void	Log(string tag, string msg)
	{
		if (s_bLogging)
		{
			s_Log += ("<color=lime>[D]</color>:"+tag + ":  <color=cyan>" + System.DateTime.Now.ToString()+"</color>\n" + msg + "\n\n");
		}
		if (s_bStdOutput)
		{
			Debug.Log(tag + ":" + msg);
		}
	}
	
	public static void	LogError(string tag, string msg)
	{
		s_Log += ("<color=red>[E]</color>:"+tag + ":  <color=cyan>" + System.DateTime.Now.ToString()+"</color>\n" + msg + "\n\n");
		Debug.LogError(tag + ":" + msg);
	}

	public static void	LogWarning(string tag, string msg)
	{
		s_Log += ("<color=yellow>[W]</color>:"+tag + ":  <color=cyan>" + System.DateTime.Now.ToString()+"</color>\n" + msg + "\n\n");
		if (s_bStdOutput)
		{
			Debug.LogWarning(tag + ":" + msg);
		}
	}

	public static void	LogOpen()
	{
		s_bVisible = true;
	}
	
	public static void	LogClose()
	{
		s_bVisible = false;
	}
	
	private static int selectedToolbar = 0;
	private static string[] selectLabel;

	private static int selectedToolbar0 = 0;
	private static string[] selectLabel0;

	class stDrawInfo
	{
		public guiDrawDelegate m_Delegate;
		public bool m_bVisible;
		public string m_Label;
		public int m_WindowID;
		public int m_nLevel;

		public stDrawInfo(int p_nWindowID, string p_Label, guiDrawDelegate p_Del, int p_nLevel)
		{
			m_bVisible = false;
			m_Label = p_Label;
			m_Delegate = p_Del;
			m_WindowID = p_nWindowID;
			m_nLevel = p_nLevel;
		}

	};

	static Dictionary<guiDrawDelegate, stDrawInfo> s_ToolbarInfo = new Dictionary<guiDrawDelegate, stDrawInfo>();
	static Dictionary<guiDrawDelegate, stDrawInfo> s_ToolbarInfo0 = new Dictionary<guiDrawDelegate, stDrawInfo>();

	public static int GUIDrawDelegateAppend(string p_Label, guiDrawDelegate p_Del, int p_nLevel = 1)
	{
		int a_nWindowID = GUIIDGet();

		stDrawInfo info = new stDrawInfo(a_nWindowID, p_Label, p_Del, p_nLevel);

		if (p_nLevel == 0)
		{
			s_ToolbarInfo0.Add(p_Del, info);

			int i = 0;
			selectLabel0 = new string[s_ToolbarInfo0.Count];
			foreach (stDrawInfo f in s_ToolbarInfo0.Values)
			{
				selectLabel0[i++] = f.m_Label;
			}
		}
		else
		{
			s_ToolbarInfo.Add(p_Del, info);

			int i = 0;
			selectLabel = new string[s_ToolbarInfo.Count];
			foreach (stDrawInfo f in s_ToolbarInfo.Values)
			{
				selectLabel[i++] = f.m_Label;
			}
		}

		return a_nWindowID;
	}

	public static void GUIDrawDelegateRemove(guiDrawDelegate p_Del)
	{
		if (s_ToolbarInfo0.ContainsKey(p_Del))
		{
			s_ToolbarInfo0.Remove(p_Del);

			int i = 0;
			selectLabel0 = new string[s_ToolbarInfo0.Count];
			foreach (stDrawInfo f in s_ToolbarInfo0.Values)
			{
				selectLabel0[i++] = f.m_Label;
			}
		}
		else
		{
			s_ToolbarInfo.Remove(p_Del);

			int i = 0;
			selectLabel = new string[s_ToolbarInfo.Count];
			foreach (stDrawInfo f in s_ToolbarInfo.Values)
			{
				selectLabel[i++] = f.m_Label;
			}
		}
	}

	//
	// デバッグウィンドウを開く
	//
	public static void Open(string name)
	{
		s_bVisible = true;

		foreach (stDrawInfo f in s_ToolbarInfo0.Values)
		{
			if (f.m_Label.Equals(name))
			{
				f.m_bVisible = true;
			}
		}

		foreach (stDrawInfo f in s_ToolbarInfo.Values)
		{
			if (f.m_Label.Equals(name))
			{
				f.m_bVisible = true;
			}
		}
	}




	static int s_GUIId = 100;
	public static int GUIIDGet()
	{
		return ++s_GUIId;
	}

	public static void GUIMatrixSet()
	{
		float s = Screen.height / c_Height;
		Vector3 sc = new Vector3(s, s, 1);

		c_Width = c_Height * ((float)Screen.width / (float)Screen.height);

		GUI.matrix = Matrix4x4.Scale(sc);
	}
	public static void GUIMatrixClear()
	{
		GUI.matrix = Matrix4x4.identity;
	}


#if _ENABLE_DEBUG_COMMAND
	void OnGUI()
	{
		GUI.skin = skin;

		GUIMatrixSet();

		if (!s_bVisible) 
		{
			Rect rect0 = new Rect(0, 0, 80, 40);

			GUIStyle a_Style = new GUIStyle();
			if (GUI.Button(rect0, "      ", a_Style))
			{
				s_bVisible = true;
			}
			GUI.skin = null;
			return;
		}

		Rect rect = new Rect(0, 0, 80, 40);

		if (GUI.Button(rect, "[閉]"))
		{
			s_bVisible = false;
			GUI.skin = null;
			return;
		}
/*
		rect.x += (rect.width + 10);

		if (GUI.Button(rect, "BTN"))
		{
			s_bVisible = false;
			GUI.skin = null;
			return;
		}
		rect.x += (rect.width + 10);

		if (GUI.Button(rect, "LOG"))
		{
			s_bVisible = false;
			GUI.skin = null;
			return;
		}
		rect.x += (rect.width + 10);

		if (GUI.Button(rect, "SOUND"))
		{
			s_bVisible = false;
			GUI.skin = null;
			return;
		}
*/


		//
		// Toolbar(常駐)
		//
		selectedToolbar0 = GUI.Toolbar(new Rect(100, 0, c_Width - 100, 40), selectedToolbar0, selectLabel0);

		if (GUI.changed)
		{
			Dictionary<guiDrawDelegate, stDrawInfo>.Enumerator ie = s_ToolbarInfo0.GetEnumerator();
			for (int i = 0; i <= selectedToolbar0; i++)
			{
				ie.MoveNext();
			}
			stDrawInfo info = ie.Current.Value;
			info.m_bVisible = !info.m_bVisible;

			GUI.changed = false;
		}

		//
		// Toolbar（シーン）
		//
		selectedToolbar = GUI.Toolbar(new Rect(0, 50, c_Width - 100, 60), selectedToolbar, selectLabel);

		if (GUI.changed)
		{
			Dictionary<guiDrawDelegate, stDrawInfo>.Enumerator ie = s_ToolbarInfo.GetEnumerator();
			for (int i = 0; i <= selectedToolbar; i++)
			{
				ie.MoveNext();
			}
			stDrawInfo info = ie.Current.Value;
			if (info != null)
			{
				info.m_bVisible = !info.m_bVisible;
			}
			GUI.changed = false;
		}

		//
		// Windows
		//
		foreach (stDrawInfo f in s_ToolbarInfo0.Values)
		{
			if (f.m_bVisible)
			{
				f.m_Delegate(f.m_WindowID);
			}
		}

		foreach (stDrawInfo f in s_ToolbarInfo.Values)
		{
			if (f.m_bVisible)
			{
				f.m_Delegate(f.m_WindowID);
			}
		}


		GUIMatrixClear();

		GUI.skin = null;
	}
#endif

	//
	// Buttons
	//
	Rect m_ButtonRect = new Rect(0, 0, 400, 100);
	public void OnGuiDrawButtonWindow(int p_WindowID)
	{
		m_ButtonRect = GUI.Window(p_WindowID, m_ButtonRect, onButtonWindow, "SWITCH");
	}

	public void onButtonWindow(int p_WindowID)
	{
		GUILayout.BeginHorizontal("box");

			/*		
			if (GUILayout.Button("END"))
			{
				s_bVisible = false;
				GameSystem.SceneLoad(defSystem.Scene.TITLE);
			}
			*/
			if (GUILayout.Button(s_bLogging ? "LOG OFF" : "LOG ON"))
			{
				s_bLogging = !s_bLogging;
			}

			if (GUILayout.Button(s_bStdOutput ? "STD OFF" : "STD ON"))
			{
				s_bStdOutput = !s_bStdOutput;
			}

		GUILayout.EndHorizontal();

		GUI.DragWindow(new Rect(0, 0, m_ButtonRect.width, m_ButtonRect.height));
	}


	//
	// LOG
	//
	Rect m_LogRect = new Rect(0, 120, 360, 480);
	Vector2 m_LogScrollPos = new Vector2(0, 0);

	void onGuiDrawLogWindow(int p_WindowID)
	{
		m_LogRect = GUI.Window(p_WindowID, m_LogRect, OnLogWindow, "LOG");
	}

	public void OnLogWindow(int p_WindowID)
	{
		GUILayout.BeginVertical("box");

			GUILayout.BeginHorizontal("box");
				if (GUILayout.Button("CLEAR"))
				{
					s_Log = "";
				}
				if (GUILayout.Button("SendEMail"))
				{
#if UNITY_ANDROID && !UNITY_EDITOR
//					EtceteraAndroid.showEmailComposer(null, "HOTDOG LOG " + System.DateTime.Now.ToString(), s_Log, false);
#endif
				}
			GUILayout.EndHorizontal();

			m_LogScrollPos = GUILayout.BeginScrollView(m_LogScrollPos);
				GUILayout.Label(s_Log);
			GUILayout.EndScrollView();

		GUILayout.EndVertical();

		GUI.DragWindow(new Rect(0, 0, m_LogRect.width, m_LogRect.height));

	}

	//
	// SCEBE
	//
	Rect m_ScnRect = new Rect(0, 120, 360, 96);
	Vector2 m_ScnScrollPos = new Vector2(0, 0);

	void onGuiDrawScnWindow(int p_WindowID)
	{
		m_ScnRect = GUI.Window(p_WindowID, m_ScnRect, OnScnWindow, "SCN");
	}

	public void OnScnWindow(int p_WindowID)
	{
		GUILayout.BeginVertical("box");

			GUILayout.BeginHorizontal("box");
				if (GUILayout.Button("TEST"))
				{
					SceneManager.LoadScene("Test", LoadSceneMode.Single);
				}
				if (GUILayout.Button("TEST2"))
				{
					SceneManager.LoadScene("Test2", LoadSceneMode.Single);
				}
				if (GUILayout.Button("TEST3"))
				{
					SceneManager.LoadScene("Test3", LoadSceneMode.Single);
				}
			GUILayout.EndHorizontal();

		GUILayout.EndVertical();

		GUI.DragWindow(new Rect(0, 0, m_ScnRect.width, m_ScnRect.height));

	}



	//
	// ファイルにログを記録
	//
	static string s_FileLogName;
	static System.Text.StringBuilder s_FileLogBuf;
	public static void FileLog_Open(string p_FileName)
	{
		s_FileLogName = p_FileName;
		s_FileLogBuf = new System.Text.StringBuilder(32768);

		s_FileLogBuf.AppendFormat("=== START At {0} ===\n", System.DateTime.Now.ToString());

	}

	public static void FileLog_Close()
	{
		if (s_FileLogName == null) return;

		s_FileLogBuf.AppendFormat("=== END At {0} ===\n", System.DateTime.Now.ToString());

		string a_Path = FileLog_PathGet() + System.IO.Path.DirectorySeparatorChar + s_FileLogName;

		TADebug.Log("Debug", "Log = " + a_Path);
		TADebug.Log("Debug", s_FileLogBuf.ToString());

		System.IO.File.WriteAllText(a_Path, s_FileLogBuf.ToString(), System.Text.Encoding.UTF8);

		s_FileLogBuf = null;
		s_FileLogName = null;
	}

	public static void FileLog_Write(string p_Str)
	{
		if (s_FileLogName == null) return;

		s_FileLogBuf.AppendFormat("{0},{1}\n", System.DateTime.Now.ToString(), p_Str);

		
	}

	/// <summary>
	/// ログファイルが保存されるパスを取得する
	/// </summary>
	/// <returns></returns>
	public static string FileLog_PathGet()
	{
		string a_Path = Application.persistentDataPath;
		return a_Path;
	}


	/// <summary>
	/// 変数の中身を文字列にする
	/// </summary>
	/// <param name="oret"></param>
	/// <param name="odat"></param>
	public static string DumpVar(System.Object oret)
	{
		string a_Res = "";
		Type t = oret.GetType();
		FieldInfo[] a_Fields = t.GetFields();

		foreach(FieldInfo f in a_Fields)
		{
			System.Object obj = f.GetValue(oret);
			if (obj != null)
			{
				if (f.FieldType == typeof(string))
				{
					a_Res += string.Format("{0} : \"{1}\"\n", f.Name, obj);
				}
				else
				{
					a_Res += string.Format("{0} : {1}　[{2}]\n", f.Name, obj.ToString(), f.FieldType.ToString());
				}
			}
			else
			{
				a_Res += string.Format("{0} : NULL\n", f.Name);
			}
		}

		return a_Res;
	}


	// ＦＰＳ計測
	public static float m_dbgMinFPS = float.MaxValue;
	public static float m_dbgMaxFPS = float.MinValue;
	public static float m_dbgAvgFPS = 0;
	public static bool  m_dbgRecordEnable = false;

	public static void dbgFPSRecord_Start()
	{
		if (s_Instance == null) return;
		dbgFPSRecord_Stop();
		s_Instance.StartCoroutine("dbgFPS_Exec");
	}
	public static void dbgFPSRecord_Stop()
	{
		if (s_Instance == null) return;
		s_Instance.StopCoroutine("dbgFPS_Exec");
	}

	/// <summary>
	/// FPSを記録する
	/// </summary>
	/// <returns></returns>
	IEnumerator dbgFPS_Exec()
	{

		int a_nFrameCount = 0;
		int a_nTotalFrameCount = 0;

		float a_StartTime = Time.time;
		float a_NextTime = Time.time + 1;

		m_dbgMinFPS = float.MaxValue;
		m_dbgMaxFPS = float.MinValue;

		while(true)
		{
	        a_nFrameCount++;
			a_nTotalFrameCount++;
 
	        if ( Time.time >= a_NextTime ) 
			{
				if (m_dbgMinFPS > a_nFrameCount)
				{
					m_dbgMinFPS = a_nFrameCount;
				}
				if (m_dbgMaxFPS < a_nFrameCount)
				{
					m_dbgMaxFPS = a_nFrameCount;
				}
				m_dbgAvgFPS = a_nTotalFrameCount / (Time.time - a_StartTime);


//				TADebug.FileLog_Write(string.Format("FPS={0} at {1}", a_nFrameCount, (Time.time - a_StartTime)));

	            a_nFrameCount = 0;
	            a_NextTime += 1;

	        }

			yield return 0;
		}

	}

	//
	// デバッグ用のタイム計測
	//
	static double m_DgbTimeStart;
	static double m_DgbTimeLast;

	/// <summary>
	/// 計測開始
	/// </summary>
	public static void TimeStart()
	{
		m_DgbTimeStart = GameUtils.UnixMilliTime(System.DateTime.Now);
		m_DgbTimeLast = m_DgbTimeStart;
	}

	/// <summary>
	/// 計測開始時点からの経過秒数
	/// </summary>
	/// <returns></returns>
	public static float TimeRap()
	{
		float a_Rap =  (float)(GameUtils.UnixMilliTime(System.DateTime.Now) - m_DgbTimeStart);
		return a_Rap;
	}






}
