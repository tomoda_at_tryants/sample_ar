using UnityEngine;
using System.Collections;

/// <summary>
/// UIRootのmanualWidthとmanualHeightを自動で調整する
/// UIRootよりも早く実行する様に、ScriptExcutionOrderの値を調整する→-300くらい）
/// </summary>
[ExecuteInEditMode]
public class AutoScreenSize : MonoBehaviour {
	
//	public bool fitHorizon = true;
	public int manualScreenWidth  = 640;
	public int manualScreenHeight = 1136;

//	[HideInInspector]
	public float offsetX = 0;
//	[HideInInspector]
	public float offsetY = 0;

	UIRoot	m_Root;
	Vector2 m_LastScreenSize;
/*
	Vector3 m_ScreenScale;
	public Vector3 screenScale
	{
		get { return m_ScreenScale; }
	}
 */ 
 
	// 
	void Awake()
	{
//		Debug.Log("AutoScreenSize::Awake()");
//		CalcHeight();
//		m_ScreenScale = new Vector3(screenSize * 2, screenSize*2, 1);
	}
/***	
	// Use this for initialization
	void Start () {
	
	}
***/	

	void OnDestroy()
	{
/*
		if (Application.isEditor && !Application.isPlaying)
		{
			m_Root.manualWidth  = manualScreenWidth;
			m_Root.manualHeight = manualScreenHeight;
		}
 */ 
	}

	void Start()
	{
		m_Root = gameObject.GetComponentInParent<UIRoot>();
		if (m_Root == null)
		{
			m_Root = gameObject.GetComponentInChildren<UIRoot>();
		}

		CalcHeight();	
	}

	// Update is called once per frame
	void Update () {

		if (Application.isEditor)
		{
			if (m_Root == null)
			{
				Start();
			}
			else
			{
				CalcHeight();	
			}
		}

	}
	
	/// <summary>
	/// スクリーンのサイズを計算する
	/// </summary>
	void CalcHeight()
	{
		if (m_Root != null)
		{
			// 前回と解像度が変わっていなかったら何もしない
			if ((m_LastScreenSize.x == Screen.width) && (m_LastScreenSize.y == Screen.height))
			{
				return;
			}
			m_LastScreenSize.x = Screen.width;
			m_LastScreenSize.y = Screen.height;

			float a_fAspect = (float)Screen.height / (float)Screen.width;

			float a_fWidth;
			float a_fHeight;

			// Fit Horizontal
			// 4:3  = 1.333
			// 3:2  = 1.5
			// 16:9 = 1.7777
			// 19:9 = 2.1111
			float a_fAspectMin = (3.0f/2.0f);
			float a_fAspectMax = (16.0f/9.0f);

			if (a_fAspect < a_fAspectMin)
			{
				// 左右をマスク
				a_fHeight = manualScreenWidth * a_fAspectMin;
				a_fWidth = a_fHeight / a_fAspect;
				offsetX = (a_fWidth - manualScreenWidth) / 2;
				offsetY = 0;
			}
			else
			if (a_fAspect > a_fAspectMax)
			{
				// 上下をマスク＆センタリング
				a_fWidth = manualScreenWidth;
				a_fHeight = (int)(a_fWidth * a_fAspect);
				offsetX = 0;
				offsetY = (a_fHeight - (a_fWidth*a_fAspectMax)) / 2;
			}
			else
			{
				// 通常（マスク無し）
				a_fHeight = (int)(manualScreenWidth * a_fAspect);
				a_fWidth = manualScreenWidth;
				offsetX = 0;
				offsetY = 0;
			}

			m_Root.manualWidth = (int)a_fWidth;
			m_Root.manualHeight = (int)a_fHeight;

//			m_Root.fitWidth = true;
//			m_Root.fitHeight = false;

//			m_ScreenScale = new Vector3(a_fWidth, a_fHeight, 1);
		
		}
#if false
		else
		{
			// Normal Camera
			Camera camera = GetComponent<Camera>();
			
			// aspect = w / h
			camera.orthographicSize = screenSize / 2;
		}
#endif
	}
#if false	
	public static Vector3 ScreenToWorld(Vector3 screenPos)
	{
		float w;
		float h;
		float a_fAspect = Screen.width / Screen.height;
		if (a_fAspect < 1.5f)
		{
			w = 480;
			h = (int)(w * Screen.height) / Screen.width;
		}
		else
		{
			h = 720;
			w = (int)(h * Screen.width) / Screen.height;
		}
		float x = screenPos.x * w / Screen.width  - (w/2);
		float y = screenPos.y * h / Screen.height - (h/2);
		
		
		Vector3 worldPos;
		worldPos.x = x;
		worldPos.y = y;
		worldPos.z = 0;
		
		return worldPos;
	}
#endif
}
