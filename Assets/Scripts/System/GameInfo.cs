﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameInfo{

	[System.NonSerializedAttribute]
	public const string	eTAG = "GameInfo";

	[System.NonSerializedAttribute]
	static GameInfo s_Instance;
	/// <summary>
	/// シングルトンインスタンス
	/// </summary>
	public static GameInfo	instance
	{
		get{ return	s_Instance; }
	}

	public static void	Create()
	{
		if (s_Instance == null)
		{
			s_Instance = new GameInfo();
			s_Instance.Load();
		}
	}

	/// <summary>
	/// 言語
	/// </summary>
	public enum eLanguage : int
	{
		NONE = -1,

		JPN,	//	日本語
		ENG,	//	英語
		PRT,	//	ポルトガル

		MAX
	}

	eLanguage	m_Language;
	/// <summary>
	/// 設定言語
	/// </summary>
	public eLanguage	language
	{
		get { return m_Language; }
	}

	/// <summary>
	/// 設定言語を切り替える
	/// </summary>
	public void	Language_Add()
	{
		m_Language++;
		if (m_Language == eLanguage.MAX)	m_Language = eLanguage.JPN;

		Save();
	}

	/// <summary>
	/// 言語の設定
	/// </summary>
	public void	Language_Set(eLanguage p_Language)
	{
		m_Language = p_Language;
		Save();
	}

    int m_nBootCount;
	/// <summary>
	/// 起動回数
	/// </summary>
	public int	bootCount
	{
		get { return m_nBootCount; }
	}

	/// <summary>
	/// 起動回数を増やす
	/// </summary>
	public void	BootCount_Add()
	{
//		m_nBootCount = 0;	//	起動時に言語選択ダイアログを表示する
		m_nBootCount++;
		Save();
	}

    Dictionary<int, int>    m_ConfigVersionList;
    public Dictionary<int, int> configVersionList
    {
        get
        {
            if(m_ConfigVersionList == null)
            {
                m_ConfigVersionList = new Dictionary<int, int>();
            }

            return m_ConfigVersionList;
        }
    }

    /// <summary>
    /// 設定ファイルを設定する
    /// </summary>
    /// <param name="p_nKey"></param>
    /// <param name="p_nValue"></param>
    public void ConfigVersion_Set(int p_nKey, int p_nValue)
    {
        //  存在しない場合は追加
        if (!m_ConfigVersionList.ContainsKey(p_nKey)) 
        {
            ConfigVersion_Add(p_nKey, p_nValue);

            return;
        }

        m_ConfigVersionList[p_nKey] = p_nValue;
        Save();
    }

    /// <summary>
    /// 設定ファイルを追加する
    /// </summary>
    /// <param name="p_nKey"></param>
    /// <param name="p_nValue"></param>
    public void ConfigVersion_Add(int p_nKey, int p_nValue)
    {
        if(m_ConfigVersionList.ContainsKey(p_nKey))    return;
        m_ConfigVersionList.Add(p_nKey, p_nValue);

        Save();
    }

    /// <summary>
    /// 初回起動か
    /// </summary>
    public bool isFirstBoot { get { return	(m_nBootCount == 1) ? true : false; } }

	/// <summary>
	/// ロード
	/// </summary>
	public void	Load()
	{
		string	a_Serialized = PlayerPrefs.GetString(eTAG);
//        a_Serialized = "";    //  データを初期化

		if (!string.IsNullOrEmpty(a_Serialized))
		{
			s_Instance = Deserialize(a_Serialized);
		}
		else
		{
			// 初回起動時
			s_Instance = new GameInfo();
			Save();
		}
	}

	/// <summary>
	/// 自身をシリアル化して返す
	/// </summary>
	/// <returns></returns>
	public string Serialize()
	{
		string	a_SerializedStr = EasySerializer.SerializeObjectToString(this);
		return	a_SerializedStr;
	}

	/// <summary>
	/// セーブデータを戻す
	/// </summary>
	/// <param name="p_SerializedStr"></param>
	/// <returns></returns>
	static GameInfo	Deserialize(string p_SerializedStr)
	{
		GameInfo	a_GameInfo = (GameInfo)EasySerializer.DeserializeObjectFromString(p_SerializedStr);
		return		a_GameInfo;
	}

	/// <summary>
	/// 自身をセーブする
	/// </summary>
	public void	Save()
	{
		if(s_Instance != null)
		{
			//	自身をシリアル化して取得
			string	a_Serialize = s_Instance.Serialize();
			// GameInfoの保存
			PlayerPrefs.SetString(eTAG, a_Serialize);
		}
	}
}
