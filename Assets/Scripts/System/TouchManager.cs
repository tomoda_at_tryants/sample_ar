﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {

	public static TouchManager	instance;

	public static TouchManager	Create()
	{
		if(instance == null)
		{
			GameObject a_Obj = new GameObject("TouchManager");
			instance = a_Obj.AddComponent<TouchManager>();

			a_Obj.AddComponent<TouchManager>();
		}
		return instance;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
