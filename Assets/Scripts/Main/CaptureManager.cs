﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;

public class CaptureManager : MonoBehaviour {

    SmartARController smartARController;
    string  m_ImageName;
    string  m_ImagePath;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// 撮影し、保存する
    /// </summary>
    public void Shot(System.Action p_Callback)
    {
        //  コンストラクタでエラーが発生すれば終了
//        if (smartARController.smart_.isConstructorFailed()) return;

        //  パスの設定
        SetPath();

        if (string.IsNullOrEmpty(m_ImageName))  return;

        //  撮影前に行う処理
        PreCaptureImage_Exec();
        //  スクリーンショット
        Application.CaptureScreenshot(m_ImageName);
        m_ImageName = "";

        //  撮影後に行われる処理
        StartCoroutine(waitUntilFinishedWriting(() => {
            onPostCaptureImage();
            if (p_Callback != null) p_Callback();
        }));
    }

    /// <summary>
    /// 保存先のパスの設定を行う
    /// </summary>
    private void    SetPath()
    {
        m_ImageName = string.Format("capture_image_{0}.png", DateTime.Now.ToString("d-MM-yyyy-HH-mm-ss-f"));
        m_ImagePath = GetImagePath(m_ImageName);
    }

    /// <summary>
    /// キャプチャしたイメージの保存先パスを取得する
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string  GetImagePath(string name)
    {
        string a_Path;
        switch (Application.platform)
        {
            case    RuntimePlatform.Android:
            case    RuntimePlatform.IPhonePlayer:
            {
                a_Path = string.Format("{0}/{1}", Application.persistentDataPath, name);
            }
            break;

            case    RuntimePlatform.WindowsPlayer:
            {
                a_Path = string.Format("{0}/{1}", Application.dataPath, name);
            }    
            break;

            default:
            {
                a_Path = name;
            }
            break;
        }

        return  a_Path;
    }

    /// <summary>
    /// 撮影前に呼ばれる処理
    /// </summary>
    void    PreCaptureImage_Exec()
    {
        //  システムのシャッター音を鳴らす
#if UNITY_IPHONE && !UNITY_EDITOR
        _PlaySystemShutterSound();
#elif UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaObject utils = new AndroidJavaObject("com.sony.smartar.utils.CaptureImageUtils"))
        {
            utils.CallStatic("playSystemShutterSound");
        }
#endif
    }

//  DllImport関連
#if UNITY_IPHONE && !UNITY_EDITOR

    /// <summary>
    /// シャッター音を再生する
    /// </summary>
    [DllImport("__Internal")]
    private static extern void _PlaySystemShutterSound();

    /// <summary>
    /// イメージを書き込む
    /// </summary>
    /// <param name="path"></param>
    [DllImport("__Internal")]
    private static extern void _WriteImageToAlbum(string path);
#endif

    /// <summary>
    /// 画像がローカルに保存されるまで待機する
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    private IEnumerator waitUntilFinishedWriting(Action callback)
    {
        while (!File.Exists(m_ImagePath))
        {
            yield return null;
        }
        callback();
        yield break;
    }

    /// <summary>
    /// 撮影後に呼ばれる処理
    /// </summary>
    void onPostCaptureImage()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
      _WriteImageToAlbum(m_ImagePath);
#elif UNITY_ANDROID && !UNITY_EDITOR
      using (AndroidJavaObject utils = new AndroidJavaObject("com.sony.smartar.utils.CaptureImageUtils"))
      {
          var scanFilePath = utils.CallStatic<string>("moveToExternalDir", m_ImagePath);
          if (string.IsNullOrEmpty(scanFilePath)) return;
          utils.CallStatic("scanCaptureImage", scanFilePath);
      }
#endif
    }
}
