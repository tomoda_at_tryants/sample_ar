﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

public class scn_main : MonoBehaviour
{
	const string eTAG = "scn_main";

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
	{
        	None,

	        State_Init,
	        State_Main,
	        State_End,

	        Max
	}

//  ステート関連の処理
#region

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	eState m_State;
	eState m_NextState;
	string m_Coroutine;

	/// <summary>
	/// ステート関連の初期化
	/// </summary>
	void State_Initialize()
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
			eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
			c_StateList.Add(a_State, a_State.ToString());
		}

		m_State		= eState.None;
		m_NextState	= eState.State_Init;
	}

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
		m_NextState = p_NextState;
        	if (p_bContinus)
        	{
			State_Check();
        	}
    	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
		if (m_NextState != m_State)
		{
    			m_State = m_NextState;
			if (!string.IsNullOrEmpty(m_Coroutine))
            		{		
				StopCoroutine(m_Coroutine);
                		m_Coroutine = null;
            		}

	            	m_Coroutine = c_StateList[m_State];
	            	StartCoroutine(m_Coroutine);

			TADebug.Log(eTAG, "Call " + m_Coroutine + "()");

	    		return true;
		}

        	return false;
	}
#endregion

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs =
    	{
//		new PrefInstantiater.Layout("Dummy",            "Dummy/Dummy",          false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero)),
		new PrefInstantiater.Layout("shotButton",       "Main/shotButton",      false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Bottom,    new Vector3(   0,  80, 0)),
        	new PrefInstantiater.Layout("backButton",       "Main/backButton",      false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Bottom,    new Vector3( 200,  90, 0)),
        	new PrefInstantiater.Layout("settingsButton",   "Main/settingsButton",  false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Bottom,    new Vector3(-200,  80, 0)),
    	};
    	PrefInstantiater        m_Instantiator;

	[SerializeField]
	Camera                  m_Camera;

	[SerializeField]
	SmartARController       m_Controller;

	List<SmartAREffector>   m_EffectorList;

	List<UIButtonTween>     m_GUIList;

	/// <summary>
	/// エフェクトアイテムリスト
	/// </summary>
	EffectItemList              m_EffectList;
    
	/// <summary>
	/// 認識していない
	/// </summary>
	bool    isNoTracking
	{
		get
		{
			if (m_EffectorList == null || m_EffectorList.Count == 0)	return false;

		    	foreach (SmartAREffector ef in m_EffectorList)
		    	{
		        	if(ef.result_.targetTrackingState_ == smartar.TargetTrackingState.TARGET_TRACKING_STATE_TRACKING)
		        	{
					return false;
		        	}
			}
	    		return true;
		}
	}

	[SerializeField]
	string  m_TrackingID;

    	UITexture           m_FrameTex;

	// Use this for initialization
	void Start()
	{
		//  ステート関連の初期化
		State_Initialize();
		//	「State_Init」を呼び出す
		State_Next(m_NextState, true);
	}

	IEnumerator State_Init()
	{
		//	共通の初期化
		bool    a_bWait = true;
		def.CommonInit(() =>
	    		{
		        	a_bWait = false;
		        	def.CommonInit_Completed();
		    	}
		);
		//	初期化が終了するまで待機
		while (a_bWait) yield return 0;

		TADebug.GUIDrawDelegateAppend(eTAG, OnGUITest);

		//	使用するプレハブをインスタンス化
		m_Instantiator = PrefInstantiater.Create();
		m_Instantiator.Build(c_Prefs);

		//  カメラとターゲットをプログラムから生成
/*
		//  ARカメラ生成
		m_Controller = m_Instantiator.Get<SmartARController>("SmartARCamera");
		//  シーン直下に移動
		m_Controller.transform.parent       = null;
		m_Controller.transform.localScale   = Vector3.one;
*/
		yield return 0;

		//  SmartARコントローラー生成
		m_Controller = ResourceManager.PrefabLoadAndInstantiate("Main/SmartARCamera", Vector3.one).GetComponent<SmartARController>();

		//	カメラ取得
		m_Camera = m_Controller.GetComponent<Camera>();

		m_EffectorList = new List<SmartAREffector>();
		foreach (ConfigTable.Data a_Data in def.instance.configTable.dataList)
		{
			GameObject		a_Obj	= new GameObject(a_Data.targetID);
    			SmartAREffector	a_Effector 	= a_Obj.AddComponent<SmartAREffector>();

		    	a_Effector.targetID			= a_Data.targetID;
		    	a_Effector.texName         		= a_Data.frameName;
		    	a_Effector._SmartARCamera	= m_Camera;

		    	m_EffectorList.Add(a_Effector);
		}

		//  エフェクトクラス生成
		RenderingEffect.Create();

		m_GUIList = new List<UIButtonTween>();

		//  撮影ボタン生成
		UIButtonTween a_Button = m_Instantiator.Get<UIButtonTween>("shotButton");
		a_Button.onClickEventSet(this, "ShotButtonDidPush", "", 0);
		m_GUIList.Add(a_Button);

		//  戻るボタン生成
		a_Button = m_Instantiator.Get<UIButtonTween>("backButton");
		a_Button.onClickEventSet(this, "BackButtonDidPush", "", 0);
		m_GUIList.Add(a_Button);

		//  戻るボタン生成
		a_Button = m_Instantiator.Get<UIButtonTween>("settingsButton");
		a_Button.onClickEventSet(this, "SettingsButtonDidPush", "", 0);
		m_GUIList.Add(a_Button);

		m_EffectList = EffectItemList.Create();
		//  開く際のコールバック
		m_EffectList.openCallback   += EffectItem_Open;
		//  閉じる際のコールバック
		m_EffectList.closeCallback  += EffectItem_Close;
		//  エフェクトが選択された際のコールバック
		m_EffectList.selectCallback += EffectItem_Select;

		State_Next(eState.State_Main, true);
	}

	/// <summary>
	/// エフェクトアイテムを開く際に呼ばれる処理
	/// </summary>
	void    EffectItem_Open()
	{
		GUI_ActiveSet(false);
	}

	/// <summary>
	/// エフェクトアイテムを閉じる際に呼ばれる処理
	/// </summary>
	void EffectItem_Close()
	{
		GUI_ActiveSet(true);
	}

	/// <summary>
	/// エフェクトアイテムが選択された際に呼ばれる処理
	/// </summary>
	void EffectItem_Select(int p_nNum)
	{
	}

	/// <summary>
	/// タイトル画面に戻るボタンが押された時の処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void BackButtonDidPush(UIButtonTween p_Sender)
	{
		StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));
	}

	public void SettingsButtonDidPush(UIButtonTween p_Sender)
	{
		//  開く
		m_EffectList.Open();
	}

	/// <summary>
	/// 撮影ボタンが押された時の処理
	/// </summary>
	/// <param name="p_Sender"></param>
	public void ShotButtonDidPush(UIButtonTween p_Sender)
	{
		//  ボタン類を非表示にする
		GUI_ActiveSet(false);

		//	撮影処理
		m_Controller.captureManager.Shot(
			//  撮影完了時に呼ばれる処理
			()=>
			{
				GUI_ActiveSet(true);
			}
		);
	}

	/// <summary>
	/// GUIの表示設定
	/// </summary>
	public void GUI_ActiveSet(bool p_bActive)
	{
		foreach(UIButtonTween a_Button in m_GUIList)
		{
	    		a_Button.gameObject.SetActive(p_bActive);
		}
	}

	IEnumerator State_Main()
	{
        	while (true)
        	{
            		//  認識チェック
            		foreach(SmartAREffector ef in m_EffectorList)
            		{
                		if (ef.isTracking && (m_FrameTex == null) && (m_TrackingID != ef.targetID))
                		{
					//  追跡IDの更新
					m_TrackingID = ef.targetID;

					//  フレームの生成
					string  a_Path  = def.c_FrameTexPath + ef.texName;

                    			m_FrameTex      = ResourceManager.TextureLoadAndInstantiate(a_Path, false);
                    			GameUtils.AttachChild(PanelManager.instance.alignmanLow.alignCenter.gameObject, m_FrameTex.gameObject);

                    			//  テクスチャのサイズに合わせる
                    			m_FrameTex.MakePixelPerfect();

	                    		//  UIWidgetの更新
	                    		UIWidget    a_Widget = m_FrameTex.GetComponent<UIWidget>();
                    			a_Widget.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnWidth;

                    			GameUtils.WidgetSize_Set(m_FrameTex.gameObject, new Vector3(def.c_ScreenSize.x, 0));

                    			m_FrameTex.gameObject.SetActive(true);
                		}
            		}

			//  認識の確認ができなかったらフレームを初期化
			if (isNoTracking)
			{
				if(m_FrameTex != null)
				{
			    		Destroy(m_FrameTex.gameObject);

			    		yield return 0;
				}

				m_TrackingID = "";
			}
            		yield return 0;
		}

		State_Next(eState.State_End, true);
	}

	IEnumerator State_End()
	{
		yield return 0;
	}

	// Update is called once per frame
	void Update()
	{
	}

// デバッグ用ＧＵＩ
#region FOR DEBUG

	Rect m_TestWinRect = new Rect(0, 130, 350, 350);
	Vector2 m_TestScrollPos = new Vector2(0, 0);

    	public void OnGUITest(int p_nWindowID)
    	{
		m_TestWinRect = GUILayout.Window(p_nWindowID, m_TestWinRect, OnGUITestWindow, eTAG);
	}

	public void OnGUITestWindow(int p_nWindowID)
	{
        	m_TestScrollPos = GUILayout.BeginScrollView(m_TestScrollPos, true, true, GUILayout.Width(320), GUILayout.Height(320));
        	{
			GUILayout.BeginVertical("box");
			{
			}
			GUILayout.EndVertical();

			GUILayout.BeginVertical("box");
			{
			}
			GUILayout.EndVertical();
        	}
        	GUILayout.EndScrollView();

        	GUI.DragWindow(new Rect(0, 0, m_TestWinRect.width, m_TestWinRect.height));
	}

#endregion

	/// <summary>
	/// 画像一覧を表示する
	/// </summary>
	public void ViewImages()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		using (AndroidJavaObject utils = new AndroidJavaObject("com.Intent.ViewGallery"))
		{
			utils.Call("showAlbum");
		}
#endif
	}
}
