﻿using UnityEngine;
using System.Collections;

public class TouchDisable : MonoBehaviour {

	static TouchDisable c_Instance;
	public static TouchDisable	instance
	{
		get { return c_Instance; }
	}

	public static bool	isDisable
	{
		get
		{
			if (c_Instance != null)
			{
				return	c_Instance.collider.enabled;
			}
			else
			{
				return	false;	
			}
		}
	}

	[SerializeField]
	BoxCollider	collider;

	public static TouchDisable	Create()
	{
		if (c_Instance != null)	return c_Instance;

		c_Instance = ResourceManager.PrefabLoadAndInstantiate<TouchDisable>("Common/TouchDisable", Vector3.zero);
		c_Instance.Init();

		GameUtils.AttachChild(PanelManager.instance.alignmanTouch.alignCenter.gameObject, c_Instance.gameObject);

		return c_Instance;
	}

	public void	SetDisable(bool p_bDisable)
	{
		collider.enabled = p_bDisable;
	}

	public void	Init()
	{
		DontDestroyOnLoad(gameObject);
	}
}
