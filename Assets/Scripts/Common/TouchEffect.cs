﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchEffect : MonoBehaviour {

	/// <summary>
	/// 生成
	/// </summary>
	public static TouchEffect	Create()
	{
		GameObject	a_Obj	= new GameObject("TouchEffect");
		m_Instantiate		= a_Obj.AddComponent<TouchEffect>();

		m_Instantiate.Init();

		return m_Instantiate;
	}

	static TouchEffect	m_Instantiate;
	public static TouchEffect	instance
	{
		get
		{
			if (m_Instantiate != null)	return m_Instantiate;
			return	Create();
		}
	}

	bool m_bView;
	/// <summary>
	/// 有効設定
	/// </summary>
	public bool	view
	{
		get { return m_bView; } 
		set
		{
			m_bView = value;

			//	エフェクトが表示されている時は全て削除
			if (!m_bView && (m_EffectList != null))
			{
				AllDestroy();
			}
		}
	}

	/// <summary>
	/// 表示されている全エフェクトを削除する
	/// </summary>
	public void	AllDestroy()
	{
		if(m_EffectList != null)
		{
			foreach (GameObject a_Effect in m_EffectList) Destroy(a_Effect);
			m_EffectList.Clear();
		}
	}

	/// <summary>
	/// 初期化
	/// </summary>
	public void	Init()
	{
		m_bView = true;

		DontDestroyOnLoad(gameObject);
	}

	List<GameObject>	m_EffectList = new List<GameObject>();
	void Update()
	{
		//	タッチされた
		if((Input.touchCount > 0 || Input.GetMouseButtonDown(0)) && m_bView)
		{
			Vector3	a_TouchPos	= Vector3.zero;
			bool	a_bEnable	= false;

			if(Input.touchCount > 0)
			{
				if(Input.touches[0].phase == TouchPhase.Began)
				{
					a_TouchPos	= Input.touches[0].position;
					a_bEnable	= true;
				}
			}
			else
			{
				a_TouchPos	= Input.mousePosition;
				a_bEnable	= true;
			}

			if(a_bEnable)
			{
				StartCoroutine(Effect_Create(a_TouchPos));
			}
		}
	}

	public IEnumerator	Effect_Create(Vector3 p_TouchPos)
	{
		if (Time.timeScale == 0)	yield break;

		// タッチした画面座標からワールド座標へ変換
		Vector3	m_WorldPos	= Camera.main.ScreenToWorldPoint(p_TouchPos);
		m_WorldPos			= new Vector3(m_WorldPos.x * 100, m_WorldPos.y * 100, 0);

		GameObject	a_Effect	= ResourceManager.PrefabLoadAndInstantiate("Common/TouchEffect", m_WorldPos, PanelManager.instance.alignmanTouch.transform);
		UISprite	a_Sp		= a_Effect.GetComponentInChildren<UISprite>();

		m_EffectList.Add(a_Effect);

		//	最大数を超えたら一番最初に生成したエフェクト
		if(m_EffectList.Count > 5)
		{
			Destroy(m_EffectList[0]);
			m_EffectList.RemoveAt(0);
		}

		yield return new WaitForSeconds(0.3f);

#if false
        StartCoroutine(GameUtils.AlphaAnim(a_Sp.color, 0.5f, 1, 0, (p_Color) =>
			{
				a_Sp.color = p_Color;
			}
		));
#endif

		// エフェクトを消す
		Destroy(a_Effect, 2);
	}
}
