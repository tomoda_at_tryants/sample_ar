﻿using UnityEngine;
using System.Collections;

public class DownloadProgress : MonoBehaviour {

    public UILabel  label;

    int m_nPointNum;
    int m_nPointCount;
    int m_nPointMax;
    int m_nInterval;

	// Use this for initialization
	void Start ()
    {
        m_nPointNum = 0;
        m_nPointMax = 3;
        m_nInterval = 20;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Animation();
	}

    public void Animation()
    {
        m_nPointCount++;
        if (m_nPointCount >= m_nInterval)
        {
            m_nPointCount = 0;

            m_nPointNum++;
            m_nPointNum %= (m_nPointMax + 1);

            string a_Str = "";
            for (int i = 0; i < m_nPointNum; i++)
            {
                a_Str += ".";
            }

            label.text = "ダウンロード中" + a_Str;
        }
    }
}
