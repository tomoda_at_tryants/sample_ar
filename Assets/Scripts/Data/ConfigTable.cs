﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class ConfigTable : MonoBehaviour
{
	public static readonly string eTAG = "ConfigTable";

	public enum eParam : int
	{
		None = -1,

		ID,
		Version,
		FrameName,
		MarkerName,
		DicName,
		Discription,
		StartDate,
		EndDate,
		Location
	}

    public class Data
    {
        public string[] paramStr;

        /// <summary>
        /// ID
        /// </summary>
        public int ID
        {
            get
            {
                int a_nValue = 0;
                string a_Param = paramStr[(int)eParam.ID];
                if (!string.IsNullOrEmpty(a_Param))
                {
                    a_nValue = int.Parse(a_Param);
                }

                return a_nValue;
            }
        }

        /// <summary>
        /// ターゲットID
        /// </summary>
        public string targetID
        {
            get
            {
                string  a_TargetID = "target_" + ID.ToString().PadLeft(4, '0');
                return  a_TargetID;
            }
        }

        /// <summary>
        /// バージョン番号
        /// </summary>
        public int version
        {
            get
            {
                int a_nValue = 0;
                string a_Param = paramStr[(int)eParam.Version];
                if (!string.IsNullOrEmpty(a_Param))
                {
                    a_nValue = int.Parse(a_Param);
                }

                return a_nValue;
            }
        }

        /// <summary>
        /// ファイル名
        /// </summary>
        public string frameName
        {
            get
            {
                string a_Param = paramStr[(int)eParam.FrameName];
                return a_Param;
            }
        }

        /// <summary>
        /// マーカー名
        /// </summary>
        public string markerName
        {
            get
            {
                string a_Param = paramStr[(int)eParam.MarkerName];
                return a_Param;
            }
        }

        /// <summary>
        /// 辞書データ
        /// </summary>
        public string dicName
        {
            get
            {
                string  a_Param = paramStr[(int)eParam.DicName];
                return  a_Param;
            }
        }

        /// <summary>
        /// 説明文
        /// </summary>
        public string discription
        {
            get
            {
                string a_Param = paramStr[(int)eParam.Discription];
                return a_Param;
            }
        }

        /// <summary>
        /// 開始日
        /// </summary>
        public string startDate
        {
            get
            {
                string a_Param = paramStr[(int)eParam.StartDate];
                return a_Param;
            }
        }

        /// <summary>
        /// 終了日
        /// </summary>
        public string endDate
        {
            get
            {
                string a_Param = paramStr[(int)eParam.EndDate];
                return a_Param;
            }
        }

        /// <summary>
        /// 場所情報
        /// </summary>
        public string location
        {
            get
            {
                string a_Param = paramStr[(int)eParam.Location];
                return  a_Param;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_Item"></param>
        public Data(string[] p_Item)
        {
            paramStr = p_Item;
        }
    }

	/// <summary>
	/// リスト
	/// </summary>
	public List<Data>	dataList;

	public void Init()
	{
		dataList = new List<Data>();
    	}

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static ConfigTable Create(bool p_bDownload, System.Action p_Callback = null)
	{
		GameObject	a_Obj	= new GameObject();
		ConfigTable	me		= a_Obj.AddComponent<ConfigTable>();

        	me.Init();

		if (p_bDownload)
		{
			//  ダウンロード処理開始
			me.Download(() =>
				{
			    		//  完了後の処理
			    		if (p_Callback != null)
			    		{
			        		p_Callback();
			        		Destroy(a_Obj);
			    		}
				}
			);
		}
		else
		{
			Destroy(a_Obj);
		}
		return me;
	}

	public void Download(System.Action p_Callback)
	{
		StartCoroutine(Download_Exec(p_Callback));
	}

	/// <summary>
	/// サーバーからダウンロードを行う
	/// </summary>
	/// <param name="p_Callback"></param>
	/// <returns></returns>
	IEnumerator   Download_Exec(System.Action p_Callback)
	{
        	//  「ConfigTable.csv」をダウンロードする
        	string  a_URL   = string.Format("{0}{1}.csv", def.c_ConfigDataURL, eTAG);
        	WWW     www     = null;

        	WWWManager.instance.Download(a_URL, eTAG, "", WWWManager.eType.None, "",
    			//  ダウンロードが完了時に呼ばれるコールバック
    			(p_WWW)=>
	            	{
	                	www = p_WWW;
	            	}
        	);

		//  ダウンロードが完了するまで待機
		while(!WWWManager.instance.isDone)
		{
	    		yield return 0;
		}

	        //  コンフィグテーブル生成（ダウンロード処理は行わない）
	        ConfigTable a_Table = Create(false);

		char[]      a_Split		= { '\n' };
		string[]    a_FieldString	= www.text.Split(a_Split);

        	int a_nIndex = 0;
        	foreach (string a_Str in a_FieldString)
        	{
            		if (!string.IsNullOrEmpty(a_Str) && a_nIndex > 0)
            		{
                		a_Split = new char[] { ',' };
                		string[]    a_Item = a_Str.Split(a_Split);

		                //  コンフィグデータを生成し、リストに追加
		                Data    a_Data = new Data(a_Item);
		                a_Table.dataList.Add(a_Data);
            		}
            		a_nIndex++;
        	}

		for (int i = 0; i < a_Table.dataList.Count; i++)
		{
            		//  ダウンロードしてきたデータ
            		Data    a_NewData = a_Table.dataList[i];

			//  ダウンロードしたデータをリストに追加
			def.instance.configTable.dataList.Add(a_NewData);

			//  既にバージョンリストに含まれている設定データか
			bool    a_bContains     = GameInfo.instance.configVersionList.ContainsKey(a_NewData.ID);
			//  バージョンアップか
			bool    a_bVersionUp    = (a_bContains) ? (GameInfo.instance.configVersionList[a_NewData.ID] < a_NewData.version) : false;

			//  新規データ
			//  設定バージョンが上がっている時はリソースを更新する
			if (!a_bContains || a_bVersionUp)
			{
		                //  バージョンリストを更新する
		                GameInfo.instance.ConfigVersion_Set(a_NewData.ID, a_NewData.version);

				//  フレーム画像のダウンロード
				a_URL = string.Format("{0}{1}.png", def.c_ConfigDataURL, a_NewData.frameName);
				WWWManager.instance.Download(a_URL, a_NewData.frameName, def.c_FrameTexPath, WWWManager.eType.Byte);

				//  マーカー画像のダウンロード
				a_URL = string.Format("{0}{1}.png", def.c_ConfigDataURL, a_NewData.markerName);
				WWWManager.instance.Download(a_URL, a_NewData.markerName, def.c_MarkerTexPath, WWWManager.eType.Byte);

				//  辞書データのダウンロード
				a_URL = string.Format("{0}{1}.dic", def.c_ConfigDataURL, a_NewData.dicName);
				WWWManager.instance.Download(a_URL, a_NewData.dicName, def.c_DicPath, WWWManager.eType.Byte, ".dic");
            		}
		}

	        //  ダウンロードが完了するまで待機
	        while(WWWManager.instance.downloadNum > 0)
	        {
			yield return 0;
	        }

        	if(p_Callback != null)  p_Callback();
	}
}
