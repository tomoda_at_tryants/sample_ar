﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// アドフリ君の情報
/// </summary>
public class AdfurikunManager : MonoBehaviour
{

#if ENABLE_ADFURIKUN

    /// <summary>
    /// ユーティリティ
    /// </summary>
    [NonSerialized]
    public AdfurikunMovieRewardUtility  util;

#endif

    [SerializeField]
    string   iPhoneAppID;
    [SerializeField]
    string   androidAppID;

#if ENABLE_ADFURIKUN

    /// <summary>
    /// 動画広告の準備ができたか
    /// </summary>
    public bool isPreparedMovieReward
    {
        get
        {
            return  util.isPreparedMovieReward();
        }
    }

#endif

    public delegate void notPreparedDelegate();
    /// <summary>
    /// 準備が未完了時に呼ばれるコールバック
    /// </summary>
    public notPreparedDelegate notPreparedCallback;

    public delegate void startPlayingDelegate();
    /// <summary>
    /// 再生開始時に呼ばれるコールバック
    /// </summary>
    public startPlayingDelegate startPlayingCallback;

    public delegate void finishedPlayingDelegate();
    /// <summary>
    /// 再生終了時に呼ばれるコールバック
    /// </summary>
    public finishedPlayingDelegate finishedPlayingCallback;

    public delegate void failedPlayingDelegate();
    /// <summary>
    /// 再生に失敗した時に呼ばれるコールバック
    /// </summary>
    public failedPlayingDelegate failedPlayingCallback;

    public delegate void adCloseDelegate();
    /// <summary>
    /// 広告が閉じられた時に呼ばれるコールバック
    /// </summary>
    public adCloseDelegate adCloseCallback;



    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
#if ENABLE_ADFURIKUN

        util        = new AdfurikunMovieRewardUtility();
        util.config = new AdfurikunMovieRewardAdConfig();

        util.config.iPhoneAppID     = iPhoneAppID;
        util.config.androidAppID    = androidAppID;

        //  ソースオブジェクトの設定
        setSourceObject(gameObject);

        //  広告の準備を開始
        if (!isPreparedMovieReward)
        {
            util.initializeMovieReward();
        }

#endif
    }

    /// <summary>
    /// ソースオブジェクトをセットする（「MovieRewardCallback」が実装されているオブジェクト）
    /// </summary>
    void setSourceObject(GameObject p_Obj)
    {
#if ENABLE_ADFURIKUN

        if (p_Obj == null) return;
        util.setMovieRewardSrcObject(p_Obj);

#endif
    }

    /// <summary>
    /// 表示
    /// </summary>
    public void Show(adCloseDelegate p_Del = null)
    {
#if ENABLE_ADFURIKUN

        if (isPreparedMovieReward)
        {
            adCloseCallback += p_Del;

            util.playMovieReward();
        }

#endif
    }

    /// <summary>
    /// 広告の状態が変化した時のコールバック
    /// </summary>
    /// <param name="vars"></param>
    void MovieRewardCallback(ArrayList vars)
    {
#if ENABLE_ADFURIKUN

        //  ステート名
        AdfurikunMovieRewardUtility.ADF_MovieStatus a_StateName = (AdfurikunMovieRewardUtility.ADF_MovieStatus)vars[0];
        //  ID
        string appID = (string)vars[1];
        //  
        string adnetworkKey = (string)vars[2];

        switch (a_StateName)
        {
            //  再生開始
            case AdfurikunMovieRewardUtility.ADF_MovieStatus.StartPlaying:
                {
                    if (startPlayingCallback != null) startPlayingCallback();
                }
                break;

            //  再生完了
            case AdfurikunMovieRewardUtility.ADF_MovieStatus.FinishedPlaying:
                {
                    if (finishedPlayingCallback != null) finishedPlayingCallback();
                }
                break;

            //  再生に失敗した
            case AdfurikunMovieRewardUtility.ADF_MovieStatus.FailedPlaying:
                {
                    if (failedPlayingCallback != null) failedPlayingCallback();
                }
                break;

            //  広告が閉じられた
            case AdfurikunMovieRewardUtility.ADF_MovieStatus.AdClose:
                {
                    if (adCloseCallback != null) adCloseCallback();

                    //  画面の回転を全て無効にする
                    Screen.orientation = ScreenOrientation.Portrait;
                    Screen.autorotateToLandscapeLeft        = false;
                    Screen.autorotateToLandscapeRight       = false;
                    Screen.autorotateToPortraitUpsideDown   = false;
                }
                break;
        }

#endif
    }
}