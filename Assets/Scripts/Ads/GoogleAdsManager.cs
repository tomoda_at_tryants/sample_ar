﻿using UnityEngine;
using System.Collections;

#if ENABLE_GOOGLEADS

using GoogleMobileAds;
using GoogleMobileAds.Api;

#endif

using System;

public class GoogleAdsManager : MonoBehaviour {

    [Serializable]
    public class AdsInfo
    {
        /// <summary>
        /// デバイス名
        /// </summary>
        [SerializeField]
        public string  device;

        /// <summary>
        /// バナー広告のID
        /// </summary>
        [SerializeField]
        public string  bannerID;

        /// <summary>
        /// インタースティシャル広告のID
        /// </summary>
        [SerializeField]
        public string  interstitialID;
    }

    //  ホットハム広告ID
    //  iOS
    //  バナー：ca-app-pub-7755058563781128/6094162891
    //  インタースティシャル:ca-app-pub-7755058563781128/7570896092

    //  うんてい
    //  バナー：ca-app-pub-4782481582877192/9691805861
    //  インタースティシャル：ca-app-pub-4782481582877192/6468539867

    /// <summary>
    /// 広告のデリゲート
    /// </summary>
    public class AdDelegate
    {
        public delegate void LoadedDelegate();
        /// <summary>
        /// ロードが完了した時に呼ばれるコールバック
        /// </summary>
        public LoadedDelegate loadedCallback;

        public delegate void FailedToLoadDelegate();
        /// <summary>
        /// ロードに失敗した時に呼ばれるコールバック
        /// </summary>
        public FailedToLoadDelegate failedToLoadCallback;

        public delegate void OpenedDelegate();
        /// <summary>
        /// 広告が開いた時に呼ばれるコールバック
        /// </summary>
        public OpenedDelegate openedCallback;

        public delegate void ClosingDelegate();
        /// <summary>
        /// 広告を閉じている時に呼ばれるコールバック
        /// </summary>
        public ClosingDelegate closingCallback;

        public delegate void ClosedDelegate();
        /// <summary>
        /// 広告を閉じた時に呼ばれるコールバック
        /// </summary>
        public ClosedDelegate closedCallback;

        public delegate void LeftApplicationDelegate();
        /// <summary>
        /// アプリから離れた時に呼ばれるコールバック
        /// </summary>
        public LeftApplicationDelegate leftApplicationCallback;
    }

    /// <summary>
    /// 広告情報
    /// </summary>
    public AdsInfo[]    adsInfo;

#if ENABLE_GOOGLEADS

    /// <summary>
    /// バナー広告
    /// </summary>
    public BannerView banner;

    /// <summary>
    /// インタースティシャル広告
    /// </summary>
    public InterstitialAd interstitial;

#endif

    /// <summary>
    /// バナー広告のデリゲート群
    /// </summary>
    public AdDelegate   bannerDel;

    /// <summary>
    /// インタースティシャル広告のデリゲート群
    /// </summary>
    public AdDelegate   interstitialDel;

    /// <summary>
    /// 現在のデバイスに応じた広告情報を取得する
    /// </summary>
    public AdsInfo  AdsInfo_CurrentDevice
    {
        get
        {
            int a_nIndex = 0;
            foreach(AdsInfo a_Ads in adsInfo)
            {
                string a_Device = "";
#if   UNITY_IPHONE
                a_Device = "iPhone";
#elif UNITY_ANDROID
                a_Device = "Android";
#endif

                if(a_Ads.device == a_Device)
                {
                    break;
                }

                a_nIndex++;
            }

            return adsInfo[a_nIndex];
        }
    }

    public void RequestBanner()
    {
#if ENABLE_GOOGLEADS

        // Create a 320x50 banner at the top of the screen.
        banner = new BannerView(AdsInfo_CurrentDevice.bannerID, AdSize.SmartBanner, AdPosition.Bottom);
        // Register for ad events.
        banner.AdLoaded             += HandleAdLoaded;
        banner.AdFailedToLoad       += HandleAdFailedToLoad;
        banner.AdOpened             += HandleAdOpened;
        banner.AdClosing            += HandleAdClosing;
        banner.AdClosed             += HandleAdClosed;
        banner.AdLeftApplication    += HandleAdLeftApplication;
        // Load a banner ad.
        banner.LoadAd(createAdRequest());

#endif
    }

    public void RequestInterstitial()
    {
#if ENABLE_GOOGLEADS

        if (def.c_bAUSmartPass) return;

        // Create an interstitial.
        interstitial = new InterstitialAd(AdsInfo_CurrentDevice.interstitialID);
        // Register for ad events.
        interstitial.AdLoaded           += HandleInterstitialLoaded;
        interstitial.AdFailedToLoad     += HandleInterstitialFailedToLoad;
        interstitial.AdOpened           += HandleInterstitialOpened;
        interstitial.AdClosing          += HandleInterstitialClosing;
        interstitial.AdClosed           += HandleInterstitialClosed;
        interstitial.AdLeftApplication  += HandleInterstitialLeftApplication;
        // Load an interstitial ad.
        interstitial.LoadAd(createAdRequest());

#endif
    }

#if ENABLE_GOOGLEADS
    // Returns an ad request with custom ad targeting.
    public AdRequest createAdRequest()
    {


        return new AdRequest.Builder()
                .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice("DeviceID")
                .AddKeyword("game")
                .SetGender(Gender.Unknown)
                .SetBirthday(new DateTime(2000, 1, 1))
                .TagForChildDirectedTreatment(false)
                .AddExtra("color_bg", "9B30FF")
                .Build();
    }
#endif

    void ShowInterstitial()
    {
#if ENABLE_GOOGLEADS

        if (def.c_bAUSmartPass) return;

        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
//            print("Interstitial is not ready yet.");
        }

#endif
    }

    /// <summary>
    /// バナー広告を表示する
    /// </summary>
    public void ShowBanner()
    {
#if ENABLE_GOOGLEADS

        if (def.c_bAUSmartPass) return;

        RequestBanner();
        banner.Show();

#endif
    }

    public void ShowInterstitial(AdDelegate.ClosedDelegate p_Del)
    {
#if ENABLE_GOOGLEADS

        if (def.c_bAUSmartPass) return;

        StartCoroutine(ShowInterstitial_Exec(p_Del));   

#endif
    }

    public IEnumerator  ShowInterstitial_Exec(AdDelegate.ClosedDelegate p_Del)
    {
#if ENABLE_GOOGLEADS

        interstitialDel.closedCallback += p_Del;

        RequestInterstitial();

        //	インタースティシャル広告のローディングが終了するまで待機
        while (!interstitial.IsLoaded())
        {
            yield return 0;
        }

        //  表示
        ShowInterstitial();

#endif

        yield return 0;
    }

#if ENABLE_GOOGLEADS

#region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        if (bannerDel.loadedCallback != null)   bannerDel.loadedCallback();
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        if (bannerDel.failedToLoadCallback != null) bannerDel.failedToLoadCallback();
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        if (bannerDel.openedCallback != null) bannerDel.openedCallback();
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
        if (bannerDel.closingCallback != null) bannerDel.closingCallback();
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        if (bannerDel.closedCallback != null) bannerDel.closedCallback();
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        if (bannerDel.leftApplicationCallback != null) bannerDel.leftApplicationCallback();
    }

#endregion

#region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        if (interstitialDel.loadedCallback != null) interstitialDel.loadedCallback();
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        if (interstitialDel.failedToLoadCallback != null) interstitialDel.failedToLoadCallback();
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        if (interstitialDel.openedCallback != null) interstitialDel.openedCallback();
    }

    void HandleInterstitialClosing(object sender, EventArgs args)
    {
        if (interstitialDel.closingCallback != null) interstitialDel.closingCallback();
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        if (interstitialDel.closedCallback != null) interstitialDel.closedCallback();
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        if (interstitialDel.leftApplicationCallback != null) interstitialDel.leftApplicationCallback();
    }

#endregion

#endif
}
