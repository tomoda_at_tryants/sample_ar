﻿using UnityEngine;
using System.Collections;

/// <summary>
/// iMobileの情報
/// </summary>
public class iMobileManager : MonoBehaviour
{
    /// <summary>
    /// テストか
    /// </summary>
    [SerializeField]
    bool    m_bTest;

    /// <summary>
    /// パートナーID
    /// </summary>
    public string   partnerId;  //  34816

    /// <summary>
    /// メディアID
    /// </summary>
    public string   mediaId;    //   135179

    /// <summary>
    /// スポットID
    /// </summary>
    public string   spotId;     //   342420

    /// <summary>
    /// 初期化が終了したか
    /// </summary>
    static bool c_bInitialized = false;

    //  デリゲート類

    public delegate void spotDidShowDelegate();
    /// <summary>
    /// 広告が表示された時に呼ばれるコールバック
    /// </summary>
    public spotDidShowDelegate spotDidShowCallback;

    public delegate void spotDidClickDelegate();
    /// <summary>
    /// 広告がクリックされた時に呼ばれるコールバック
    /// </summary>
    public spotDidClickDelegate spotDidClickCallback;

    public delegate void spotDidCloseDelegate();
    /// <summary>
    /// 広告が閉じられた時に呼ばれるコールバック
    /// </summary>
    public spotDidCloseDelegate spotDidCloseCallback;

    public delegate void spotDidFailDelegate();
    /// <summary>
    /// 広告の取得に失敗した時に呼ばれるコールバック
    /// </summary>
    public spotDidFailDelegate spotDidFailCallback;

    /// <summary>
    /// テストモードの起動時の設定
    /// </summary>
    public void Init()
    {
        //  初回起動時のみ実行
        if (!c_bInitialized)
        {
#if ENABLE_IMOBILE

            //  テストモード設定
            IMobileSdkAdsUnityPlugin.setTestMode(m_bTest);

            IMobileSdkAdsUnityPlugin.addObserver(gameObject.name);

            IMobileSdkAdsUnityPlugin.registerFullScreen(partnerId, mediaId, spotId);
            IMobileSdkAdsUnityPlugin.start(spotId);

            c_bInitialized = true;

#endif
        }
    }

    /// <summary>
    /// 表示
    /// </summary>
    public void Show(spotDidCloseDelegate p_Del)
    {
        if (!c_bInitialized)    return;

        //  コールバック設定
        spotDidCloseCallback += p_Del;

#if ENABLE_IMOBILE

        // Show()を呼び、広告を表示します
        IMobileSdkAdsUnityPlugin.show(spotId);

#endif
    }

    /// <summary>
    /// 広告が表示された時に呼ばれるコールバック
    /// </summary>
    /// <param name="value"></param>
    void imobileSdkAdsSpotDidShow(string value)
    {
        if (spotDidShowCallback != null) spotDidShowCallback();
    }

    /// <summary>
    /// 広告がクリックされた時に呼ばれるコールバック
    /// </summary>
    /// <param name="value"></param>
    void imobileSdkAdsSpotDidClick(string value)
    {
        if (spotDidClickCallback != null) spotDidClickCallback();
    }

    /// <summary>
    /// 広告が閉じられた時に呼ばれるコールバック
    /// </summary>
    /// <param name="value"></param>
    void imobileSdkAdsSpotDidClose(string value)
    {
        if (spotDidCloseCallback != null) spotDidCloseCallback();
    }

    /// <summary>
    /// 広告の取得に失敗した時に呼ばれるコールバック
    /// </summary>
    /// <param name="spotIdAndFailReason"></param>
    void imobileSdkAdsSpotDidFail(string spotIdAndFailReason)
    {
        if (spotDidFailCallback != null) spotDidFailCallback();
    }
}