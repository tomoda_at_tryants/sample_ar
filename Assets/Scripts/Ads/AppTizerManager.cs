﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AppTizerManager : MonoBehaviour {

    public string   URL = "hamutower://";

    /// <summary>
    /// 始めてアプリを起動した時に一度だけブラウザが起動され指定のページを表示する
    /// （このタイミングで成果計測が行われる）
    /// </summary>
    public void ViewWebPage()
    {
#if   UNITY_EDITOR
        return;
#endif

#if UNITY_ANDROID
        AndroidJavaClass    a_UnityPlayer   = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject   a_Activity      = a_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject   context         = a_Activity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaObject   a_AdManager     = new AndroidJavaObject("jp.APPTIZER.android.AdManager", context);

        a_AdManager.Call("sendConversion", URL);

#elif UNITY_IPHONE
        //[DllImport("__Internal")]private static extern void sendConversionWithStartPage(string url);
        sendConversionWithStartPage(URL);
#endif
    }

}
