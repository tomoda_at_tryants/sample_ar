﻿using UnityEngine;
using System.Collections;

public class AdManager : MonoBehaviour {

    const string    eTAG = "AdManager";

    static AdManager c_Instance;
    /// <summary>
    /// シングルトンインスタンス
    /// </summary>
    public static AdManager instance
    {
        get
        {
            return c_Instance;
        }
    }

    [SerializeField]
    AdfurikunManager m_AdfurikunManager;
    /// <summary>
    /// アドフリくん
    /// </summary>
    public AdfurikunManager adfurikunManager
    {
        get { return m_AdfurikunManager; }
        set { m_AdfurikunManager = value; }
    }

    [SerializeField]
    iMobileManager m_iMobileManager;
    /// <summary>
    /// iMobile
    /// </summary>
    public iMobileManager iMobileManager
    {
        get { return m_iMobileManager; }
        set { m_iMobileManager = value; }
    }

    [SerializeField]
    GoogleAdsManager m_GoogleAdsManager;
    /// <summary>
    /// GoogleAds
    /// </summary>
    public GoogleAdsManager googleAdsManager
    {
        get { return m_GoogleAdsManager; }
        set { m_GoogleAdsManager = value; }
    }

    [SerializeField]
    AppTizerManager m_AppTizerManager;
    /// <summary>
    /// AppTizer
    /// </summary>
    public AppTizerManager appTizerManager
    {
        get { return m_AppTizerManager; }
        set { m_AppTizerManager = value; }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static AdManager Create()
    {
        GameObject  a_Obj   = ResourceManager.PrefabLoadAndInstantiate(string.Format("Prefabs/Systems/{0}", eTAG), Vector3.zero);
        c_Instance      = a_Obj.GetComponent<AdManager>();

        c_Instance.Init();

        return c_Instance;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        //  アドフリくんマネージャ追加
        if(m_AdfurikunManager != null)
        {
            m_AdfurikunManager.Init();
        }

        //  iMobileマネージャ追加
        if(m_iMobileManager != null)
        {
            m_iMobileManager.Init();
        }

        DontDestroyOnLoad(gameObject);
    }
}