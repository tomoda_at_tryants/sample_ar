using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class CameraEffector : MonoBehaviour {
	// Public property
	public string targetID = null;

	private SmartARController smartARController_;
	private IntPtr landmarkBuffer_ = IntPtr.Zero;
	private IntPtr initPointBuffer_ = IntPtr.Zero;
	private smartar.RecognitionResult result_;
	private float nearClipPlane_;

	private void DoCreate() {
		if (landmarkBuffer_ != IntPtr.Zero) {
			return;
		}
		landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
		initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);
	}
	
	private void DoEnable() {
		// Find SmartARController
        if (smartARController_ != null) 
        {
            return;
        }
		var controllers = (SmartARController[]) FindObjectsOfType(typeof(SmartARController));
		if (controllers != null && controllers.Length > 0) {
			smartARController_ = controllers[0];
		}

		result_ = new smartar.RecognitionResult();
		result_.maxLandmarks_ = smartar.Recognizer.MAX_NUM_LANDMARKS;
		result_.landmarks_ = landmarkBuffer_;
		result_.maxInitPoints_ = smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS;
		result_.initPoints_ = initPointBuffer_;
	}
	
	private void DoDestroy() {
		if (landmarkBuffer_ == IntPtr.Zero) {
			return;
		}
		
		Marshal.FreeCoTaskMem(landmarkBuffer_);
		landmarkBuffer_ = IntPtr.Zero;
		Marshal.FreeCoTaskMem(initPointBuffer_);
		initPointBuffer_ = IntPtr.Zero;
	}

	void Awake () {
		DoCreate();
	}
	
	void Start () {
        DoEnable();
		nearClipPlane_ = Camera.main.nearClipPlane * 1000;
	}
	
	void OnDestroy () {
		DoDestroy();
	}
	
	void OnApplicationQuit () {
		DoDestroy();
	}

	void Update () {

        if (smartARController_ == null)
        {
            return;
        }

		if (!smartARController_.enabled_) {
			return;
		}

		// Get recognition result
		smartARController_.GetResult(targetID, ref result_);

        // Set pose
		if (result_.isRecognized_) {

			// set position and rotation for camera
			setPose(transform, result_);
		}
	}

    void OnGUI()
    {
        DoEnable();
    }

	private void setPose(Transform transformObject, smartar.RecognitionResult result)
	{
		smartar.Vector3 zeroPos;
		zeroPos.x_ = 0f;
		zeroPos.y_ = 0f;
		zeroPos.z_ = 0f;
		setPose (transformObject, result, zeroPos);
	}

	public void setPose(Transform transformObject, smartar.RecognitionResult result, smartar.Vector3 landmarkPos)
	{
//		transformObject.rotation = Quaternion.identity;
//		transformObject.Rotate(new Vector3(0, -90, 0));
//		transformObject.Rotate(new Vector3(90, 0, 0));
//		transformObject.position = new Vector3(
//			(result.position_.x_ + landmarkPos.x_) * nearClipPlane_,
//			(result.position_.y_ + landmarkPos.z_) * nearClipPlane_,
//			(result.position_.z_ + landmarkPos.y_) * nearClipPlane_);
//		transformObject.rotation = transformObject.rotation * new Quaternion(result.rotation_.z_, result.rotation_.w_, result.rotation_.y_, result.rotation_.x_);

		smartar.Vector3 rotPosition;
		smartar.Quaternion rotRotation;
		smartARController_.adjustPose(result.position_, result.rotation_, out rotPosition, out rotRotation);

		transformObject.rotation = Quaternion.identity;
		transformObject.RotateAround(Vector3.zero, Vector3.right, -90);

		var q = new Quaternion(rotRotation.x_, rotRotation.z_, rotRotation.y_, rotRotation.w_);
		float angle;
		Vector3 axis;
		q.ToAngleAxis(out angle, out axis);
		transformObject.RotateAround(Vector3.zero, axis, angle);

		transformObject.position = new Vector3(
			(rotPosition.x_ + landmarkPos.y_) * nearClipPlane_,
			(rotPosition.z_ + landmarkPos.z_) * nearClipPlane_,
			(rotPosition.y_ + landmarkPos.x_) * nearClipPlane_);
    }
}
