﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class InitPointEffector : MonoBehaviour
{
    public GameObject sphere_;

    private SmartARController smartARController_;
    private IntPtr initPointBuffer_ = IntPtr.Zero;
    private SmartAREffector smartAREffector_;
    private TargetEffector targetEffector_;
    private smartar.RecognitionResult result_;
    private GameObject[] initPointObjects_ = new GameObject[smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS];
    private GUIStyle style_;
    private GUIStyleState styleState_;

    private struct initPointPos
    {
        public uint id_;
        public Vector2 adjustedScreenPos_;
    }

    private initPointPos[] initPointIDs_ = new initPointPos[smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS];

    void DoEnable()
    {
        // Find SmartARController
        if (smartARController_ != null)
        {
            return;
        }
        var controllers = (SmartARController[])FindObjectsOfType(typeof(SmartARController));
        if (controllers != null && controllers.Length > 0)
        {
            smartARController_ = controllers[0];
        }

        // Find SmartARController
        if (smartAREffector_ != null)
        {
            return;
        }
        var smartAREffectors = (SmartAREffector[])FindObjectsOfType(typeof(SmartAREffector));
        if (smartAREffectors != null && smartAREffectors.Length > 0)
        {
            smartAREffector_ = smartAREffectors[0];
        }

        if (targetEffector_ != null)
        {
            return;
        }
        var targetEffectors = FindObjectsOfType<TargetEffector>();
        if (targetEffectors != null && targetEffectors.Length > 0)
        {
            targetEffector_ = targetEffectors[0];
        }
    }

    void Awake()
    {
        initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);
        for (int i = 0; i < smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS; i++)
        {
            initPointObjects_[i] = (GameObject)Instantiate(sphere_, new Vector3(), Quaternion.identity);
            initPointObjects_[i].transform.localScale = new Vector3(2f, 2f, 2f);
            initPointObjects_[i].SetActive(false);
            initPointObjects_[i].transform.parent = transform;
        }

    }

    void Start()
    {
        DoEnable();
        result_ = new smartar.RecognitionResult();
        result_.maxInitPoints_ = smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS;
        result_.initPoints_ = initPointBuffer_;

        style_ = new GUIStyle();
        style_.fontSize = 30;

        styleState_ = new GUIStyleState();
        styleState_.textColor = Color.yellow;
    }

    void OnGUI()
    {
        DoEnable();

        // show initpoint id
        for (int i = 0; i < result_.maxInitPoints_; i++) {
            if (i < result_.numInitPoints_)
            {
                Vector2 screenSize = new Vector2(Screen.width, Screen.height);
                Rect rect = new Rect(initPointIDs_[i].adjustedScreenPos_.x, screenSize.y - initPointIDs_[i].adjustedScreenPos_.y, 100, 50);
                style_.normal = styleState_;
                GUI.Label(rect, initPointIDs_[i].id_.ToString(), style_);
            }
        }
    }

    void Update()
    {
        if (smartARController_ == null || smartAREffector_ == null)
        {
            return;
        }

        if (!smartARController_.enabled_)
        {
            return;
        }

        if (smartARController_.smart_.isConstructorFailed()) { return; }

        // Get recognition result
        smartARController_.GetResult(targetEffector_.targetID, ref result_);

        // Draw initPoints in unity
        if (result_.numInitPoints_ > 0)
        {
            IntPtr initPointPtr = result_.initPoints_;

            // for drawing
            for (int i = 0; i < result_.maxInitPoints_; i++)
            {
                // get a current initPoint
                smartar.InitPoint curInitPoint = (smartar.InitPoint)Marshal.PtrToStructure(initPointPtr, typeof(smartar.InitPoint));
                initPointIDs_[i].id_ = curInitPoint.id_;
                if (i < result_.numInitPoints_)
                {
                    // set position and rotation for initPoint
                    Camera camera = smartARController_.GetComponent<Camera>();

                    // Scaling
                    Vector2 videoSize = smartARController_.cameraDeviceSettings_.videoImageSize;
                    //Debug.Log("videoSize = (" + videoSize.x + ", " + videoSize.y + ")");
                    Vector2 screenSize = new Vector2(Screen.width, Screen.height);
                    //Debug.Log("screenSize = (" + screenSize.x + ", " + screenSize.y + ")");
                    float adjustRatio = (float)screenSize.x / (float)videoSize.x;
                    float adjustHeight = ((float)videoSize.y * (float)adjustRatio - (float)screenSize.y) / 2.0f;

                    initPointIDs_[i].adjustedScreenPos_.x = curInitPoint.position_.x_ * adjustRatio;
                    initPointIDs_[i].adjustedScreenPos_.y = curInitPoint.position_.y_ * adjustRatio - adjustHeight;

                    initPointObjects_[i].transform.position = camera.ScreenToWorldPoint(
                        new Vector3(initPointIDs_[i].adjustedScreenPos_.x, initPointIDs_[i].adjustedScreenPos_.y, camera.transform.position.z));
                    initPointObjects_[i].GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                    initPointObjects_[i].SetActive(true);
                    //Debug.Log("curInitPoint = (" + curInitPoint.position_.x_ + ", " + curInitPoint.position_.y_ + ")");
                    //Debug.Log("initPointObjects_[" + i + "].transform.position = " + initPointObjects_[i].transform.position);
                }
                else
                {
                    initPointObjects_[i].SetActive(false);
                }

                // go to a next ptr
                initPointPtr = new IntPtr(initPointPtr.ToInt64() + (Int64)Marshal.SizeOf(curInitPoint));
            }
        }
        else
        {
            for (int i = 0; i < smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS; i++)
            {
                initPointObjects_[i].SetActive(false);
            }
        }
    }
}
