﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class LandmarkEffector : MonoBehaviour
{
    public GameObject sphere_;

    private SmartARController smartARController_;
    private IntPtr landmarkBuffer_ = IntPtr.Zero;
    private IntPtr nodePointBuffer_ = IntPtr.Zero;
    private SmartAREffector smartAREffector_;
    private TargetEffector targetEffector_;
    private smartar.RecognitionResult result_;
    private GameObject[] landmarkObjects_ = new GameObject[smartar.Recognizer.MAX_NUM_LANDMARKS];
    private GameObject[] nodePointObjects_ = new GameObject[smartar.Recognizer.MAX_NUM_NODE_POINTS];

    void DoEnable()
    {
        // Find SmartARController
        if (smartARController_ != null)
        {
            return;
        }
        var controllers = (SmartARController[])FindObjectsOfType(typeof(SmartARController));
        if (controllers != null && controllers.Length > 0)
        {
            smartARController_ = controllers[0];
        }

        // Find SmartARController
        if (smartAREffector_ != null)
        {
            return;
        }
        var smartAREffectors = (SmartAREffector[])FindObjectsOfType(typeof(SmartAREffector));
        if (smartAREffectors != null && smartAREffectors.Length > 0)
        {
            smartAREffector_ = smartAREffectors[0];
        }

        if (targetEffector_ != null)
        {
            return;
        }
        var targetEffectors = FindObjectsOfType<TargetEffector>();
        if (targetEffectors != null && targetEffectors.Length > 0)
        {
            targetEffector_ = targetEffectors[0];
        }
    }

    void Awake()
    {
        landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
        nodePointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.NodePoint)) * smartar.Recognizer.MAX_NUM_NODE_POINTS);
        for (int i = 0; i < smartar.Recognizer.MAX_NUM_LANDMARKS; i++)
        {
            landmarkObjects_[i] = (GameObject)Instantiate(sphere_, new Vector3(), Quaternion.identity);
            landmarkObjects_[i].transform.localScale = new Vector3(20f, 20f, 20f);
            landmarkObjects_[i].SetActive(false);
            landmarkObjects_[i].transform.parent = transform;
        }
        for (int i = 0; i < smartar.Recognizer.MAX_NUM_NODE_POINTS; i++)
        {
            nodePointObjects_[i] = (GameObject)Instantiate(sphere_, new Vector3(), Quaternion.identity);
            nodePointObjects_[i].transform.localScale = new Vector3(20f, 20f, 20f);
            nodePointObjects_[i].SetActive(false);
            nodePointObjects_[i].transform.parent = transform;
        }
    }

    void Start()
    {
        DoEnable();
        result_ = new smartar.RecognitionResult();
        result_.maxLandmarks_ = smartar.Recognizer.MAX_NUM_LANDMARKS;
        result_.landmarks_ = landmarkBuffer_;
        result_.maxNodePoints_ = smartar.Recognizer.MAX_NUM_NODE_POINTS;
        result_.nodePoints_ = nodePointBuffer_;
    }

    void OnGUI()
    {
        DoEnable();
    }

    void Update()
    {

        if (smartARController_ == null || smartAREffector_ == null)
        {
            return;
        }

        if (!smartARController_.enabled_)
        {
            return;
        }

        if (smartARController_.smart_.isConstructorFailed()) { return; }

        // Get recognition result
        smartARController_.GetResult(targetEffector_.targetID, ref result_);

        // Set pose
        if (result_.isRecognized_)
        {
            // Draw landmarks in unity
            if (result_.numLandmarks_ > 0)
            {
                IntPtr landmarkPtr = result_.landmarks_;

                // for drawing
                Color landmarkColor;

                for (int i = 0; i < result_.maxLandmarks_; i++)
                {
                    // get a current landmark
                    smartar.Landmark curLandmark = (smartar.Landmark)Marshal.PtrToStructure(landmarkPtr, typeof(smartar.Landmark));

                    if (i < result_.numLandmarks_)
                    {
                        // set position and rotation for landmarks
                        smartAREffector_.setPose(landmarkObjects_[i].transform, result_);
                        landmarkObjects_[i].transform.position = landmarkObjects_[i].transform.position - smartAREffector_.transform.position;

                        // set color
                        switch (curLandmark.state_)
                        {
                            case smartar.LandmarkState.LANDMARK_STATE_TRACKED:
                                landmarkColor = new Color(0.0f, 1.0f, 0.0f);
                                break;
                            case smartar.LandmarkState.LANDMARK_STATE_LOST:
                                landmarkColor = new Color(1.0f, 0.0f, 0.0f);
                                break;
                            case smartar.LandmarkState.LANDMARK_STATE_SUSPENDED:
                                landmarkColor = new Color(0.0f, 1.0f, 1.0f);
                                break;
                            case smartar.LandmarkState.LANDMARK_STATE_MASKED:
                                landmarkColor = new Color(1.0f, 1.0f, 0.0f);
                                break;
                            default:
                                landmarkColor = new Color(0.0f, 0.0f, 0.0f);
                                break;
                        }
                        landmarkObjects_[i].GetComponent<Renderer>().material.SetColor("_Color", landmarkColor);
                        landmarkObjects_[i].SetActive(true);
                        //Debug.Log ("landmarkObjects_[" + i + "].transform.position = " + landmarkObjects_[i].transform.position);
                    }
                    else
                    {
                        landmarkObjects_[i].SetActive(false);
                    }

                    // go to a next ptr
                    landmarkPtr = new IntPtr(landmarkPtr.ToInt64() + (Int64)Marshal.SizeOf(curLandmark));
                }
            }

            // Draw nodePoints in unity
            if (result_.numNodePoints_ > 0)
            {
                IntPtr nodePointPtr = result_.nodePoints_;

                // for drawing
                Color nodePointColor = new Color(1.0f, 1.0f, 1.0f);

                for (int i = 0; i < result_.maxNodePoints_; i++)
                {
                    // get a current nodePoint
                    smartar.NodePoint curNodePoint = (smartar.NodePoint)Marshal.PtrToStructure(nodePointPtr, typeof(smartar.NodePoint));

                    if (i < result_.numNodePoints_)
                    {
                        // set position and rotation for nodePoint
                        smartAREffector_.setPose(nodePointObjects_[i].transform, result_);
                        nodePointObjects_[i].transform.position = nodePointObjects_[i].transform.position - smartAREffector_.transform.position;
                        nodePointObjects_[i].GetComponent<Renderer>().material.SetColor("_Color", nodePointColor);
                        nodePointObjects_[i].SetActive(true);
                        //Debug.Log ("nodePointObjects_[" + i + "].transform.position = " + nodePointObjects_[i].transform.position);
                    }
                    else
                    {
                        nodePointObjects_[i].SetActive(false);
                    }

                    // go to a next ptr
                    nodePointPtr = new IntPtr(nodePointPtr.ToInt64() + (Int64)Marshal.SizeOf(curNodePoint));
                }
            }
        }
        else
        {
            for (int i = 0; i < smartar.Recognizer.MAX_NUM_LANDMARKS; i++)
            {
                landmarkObjects_[i].SetActive(false);
            }
            for (int i = 0; i < smartar.Recognizer.MAX_NUM_NODE_POINTS; i++)
            {
                nodePointObjects_[i].SetActive(false);
            }
        }
    }
}
