﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class TargetEffector : MonoBehaviour
{
    public string targetID;

    [HideInInspector]
    public smartar.RecognitionResult result_;

    [SerializeField]
    private int m_LostPermissionCount = 0;

    private IntPtr landmarkBuffer_ = IntPtr.Zero;
    private IntPtr initPointBuffer_ = IntPtr.Zero;
    private SmartARController smartARController_;
    private int m_LostCount = int.MinValue;

    void Awake()
    {
        smartARController_ = FindObjectsOfType<SmartARController>()[0];
        landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
        initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);
    }

    void Start()
    {
        if (smartARController_.smart_.isConstructorFailed()) {
            showOrHideChildrens(false);
            return;
        }
        bool active = false;
        foreach (var target in smartARController_.recognizerSettings_.targets)
        {
            if (targetID == target.id)
            {
                active = true;
            }
        }
        gameObject.SetActive(active);

        // For scene mapping mode which not use targets
        bool isRecognittionModeSceneMapping = 
            smartARController_.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING;
        bool isSceneMappingInitModeTarget =
            smartARController_.recognizerSettings_.sceneMappingInitMode == smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET;
        if (isRecognittionModeSceneMapping && !isSceneMappingInitModeTarget)
        {
            targetID = null;
        }

        result_ = new smartar.RecognitionResult();
        result_.maxLandmarks_ = smartar.Recognizer.MAX_NUM_LANDMARKS;
        result_.landmarks_ = landmarkBuffer_;
        result_.maxInitPoints_ = smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS;
        result_.initPoints_ = initPointBuffer_;

        showOrHideChildrens(false);
    }

    void Update()
    {
        if (smartARController_ == null) { return; }
        if (!smartARController_.enabled_) { return; }
        if (smartARController_.smart_.isConstructorFailed()) { return; }

        // For load scene mapping mode.
        if (smartARController_.isLoadSceneMap_ && targetID != null)
        {
            if (targetID != smartARController_.recognizerSettings_.targets[0].id) {
                gameObject.SetActive(false);
                return;
            }
            targetID = null;
        }

        if (result_.isRecognized_)
        {
            m_LostCount = 0;
        }
        else
        {
            if (m_LostCount == int.MinValue) { return; }
            ++m_LostCount;
        }
        var isShown = m_LostCount <= m_LostPermissionCount;
        showOrHideChildrens(isShown);
    }

    void OnDestroy()
    {
        if (landmarkBuffer_ == IntPtr.Zero) { return; }
        Marshal.FreeCoTaskMem(landmarkBuffer_);
        landmarkBuffer_ = IntPtr.Zero;
        Marshal.FreeCoTaskMem(initPointBuffer_);
        initPointBuffer_ = IntPtr.Zero;
    }

    void OnValidate()
    {
        m_LostPermissionCount = m_LostPermissionCount < 0 ? 0 : m_LostPermissionCount;
    }

    void showOrHideChildrens(bool enabled)
    {
        var renderers = GetComponentsInChildren<Renderer>();
        foreach (var renderer in renderers)
        {
            renderer.enabled = enabled;
        }
    }
}
