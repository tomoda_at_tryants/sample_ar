﻿using UnityEngine;
using System;
using System.Collections;

public class AppControllerForLoadMap : AppControllerBase
{
    private const string SCENE_MAP_FILE_NAME = "/scenemap.dat";

    private string _SceneMapFilePath;
    private bool _IsFix;

    void Start()
    {
        _SceneMapFilePath = Application.persistentDataPath + SCENE_MAP_FILE_NAME;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnGUI()
    {
        base.OnGUI();

        if (smartARController.smart_.isConstructorFailed()) { return; }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        mySkin.button.fontSize = 15;
        mySkin.label.fontSize = 15;
#endif

        var buttonLongSide = Math.Max((float)Screen.width * 0.1f, (float)Screen.height * 0.1f);
        var buttonNarrowSide = Math.Min((float)Screen.width * 0.1f, (float)Screen.height * 0.1f);

        if (GUI.Button(new Rect(0, Screen.height - buttonNarrowSide, buttonLongSide, buttonNarrowSide), "Reset"))
        {
            smartARController.restartController();
        }

        var width = buttonLongSide * 2;
        var height = buttonNarrowSide;
        if (GUI.Button(new Rect(Screen.width - width, 0, width, height), "SaveSceneMap"))
        {
            var fileStreamOut = new smartar.FileStreamOut(smartARController.smart_, _SceneMapFilePath);
            smartARController.recognizer_.SaveSceneMap(fileStreamOut);
        }

        if (GUI.Button(new Rect(Screen.width - width, height, width, height), "LoadSceneMap"))
        {
            smartARController.loadSceneMap(_SceneMapFilePath);
            _IsFix = true;
        }

        if (GUI.Button(new Rect(Screen.width - width, height * 2, width, height), "FixSceneMap"))
        {
            _IsFix = !_IsFix;
            smartARController.recognizer_.FixSceneMap(_IsFix);
        }
    }
}
