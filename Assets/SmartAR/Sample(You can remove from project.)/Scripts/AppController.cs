﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AppController : AppControllerBase
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnGUI()
    {
        base.OnGUI();

        if (smartARController.smart_.isConstructorFailed()) { return; }

        if (GUI.Button(new Rect(0, Screen.height - 100, 200, 100), "Reset"))
        {
            smartARController.restartController();
        }
    }
}
