﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AppControllerForFull : AppControllerBase
{
    [SerializeField]
    private Texture2D red;
    [SerializeField]
    private Texture2D green;
    [SerializeField]
    private Texture2D cyan;
    [SerializeField]
    private Texture2D yerrow;
    [SerializeField]

    // Const
    private const int VGA_WIDTH = 640;
    private const int VGA_HEIGHT = 480;
    private readonly smartar.Size VGA = new smartar.Size(VGA_WIDTH, VGA_HEIGHT);
    private const smartar.FocusMode DEFAULT_FOCUS_MODE_FIRST = smartar.FocusMode.FOCUS_MODE_CONTINUOUS_AUTO_VIDEO;
    private const smartar.FocusMode DEFAULT_FOCUS_MODE_SECOND = smartar.FocusMode.FOCUS_MODE_INFINITY;
    private const smartar.FocusMode DEFAULT_FOCUS_MODE_THIRD = smartar.FocusMode.FOCUS_MODE_MANUAL;
    private const smartar.FlashMode DEFAULT_FLASH_MODE = smartar.FlashMode.FLASH_MODE_AUTO;
    private const smartar.ExposureMode DEFAULT_EXPOSURE_MODE = smartar.ExposureMode.EXPOSURE_MODE_CONTINUOUS_AUTO;
    private const smartar.WhiteBalanceMode DEFAULT_WHITE_BALANCE_MODE = smartar.WhiteBalanceMode.WHITE_BALANCE_MODE_CONTINUOUS_AUTO;
    private const smartar.SceneMode DEFAULT_SCENE_MODE = smartar.SceneMode.SCENE_MODE_SPORTS;
    private const float BEST_FPS = 30.0f;
    private const string SCENE_MAP_FILE_NAME = "/scenemap.dat";
    private GUIStyle menuButtonStyle;
    private GUIStyle selectedMenuButtonStyle;
    private GUIStyle cannotSelectMenuButtonStyle;
    private static string labelText;
    private bool isMenuShow = false;
    private bool enabled_ = false;
#pragma warning disable 414
    private bool prefEnabled_ = false;
#pragma warning restore 414

    // For preferences.
    private const String PREFERENCE_KEY_SEARCH_POLICY = "SearchPolicy";
    private const String PREFERENCE_KEY_DENSE_MAP_MODE = "DenseMapMode";
    private const String PREFERENCE_KEY_STILL_IMAGE_SIZE = "StillImageSize";
    private const String PREFERENCE_KEY_FPS_RANGE = "FpsRange";
    private const String PREFERENCE_KEY_FOCUS_MODE = "FocusMode";
    private const String PREFERENCE_KEY_FLASH_MODE = "FlashMode";
    private const String PREFERENCE_KEY_EXPOSURE_MODE = "ExposureMode";
    private const String PREFERENCE_KEY_WHITE_BALANCE_MODE = "BalanceMode";
    private const String PREFERENCE_KEY_SCENE_MODE = "SceneMode";
    private const String PREFERENCE_KEY_SENSOR_DEVICE = "SensorDevice";
    private const String PREFERENCE_KEY_REMOVE_LOST_LANDMARKS = "RemoveLostLandMarks";
    private const String PREFERENCE_KEY_TRIANGULATE_MASK = "TriangulateMask";
    private const String PREFERENCE_KEY_SHOW_STATUS = "ShowStatus";

    //For fps view.
    private const float UPDATE_INTERVAL = 1.0f;
    private ulong drawFrameCount_ = 0;
    private float lastUpdateTime_ = 0.0f;
    private float drawFrameFps_ = 0.0f;
    private float[] recognizerFps_ = new float[2];
    private float cameraFrameFps_ = 0.0f;
    private float drawFrameTime_ = 0.0f;
    private float[] recognizerTime_ = new float[2];
    private ulong lastCameraFrameCount_ = 0;
    private ulong[] lastRecogCount_ = new ulong[2];
    private ulong[] lastRecogTime_ = new ulong[2];
    private ulong lastDrawFrameCount_ = 0;
    private ulong lastDrawTime_ = 0;

    private smartar.TargetTrackingState targetTrackingStateResult_;
    private smartar.SceneMappingState sceneMappingStateresult_;

    // For status view
    private bool removeLostLandmarks_ = false;
    private int trackedLandmarkCount_ = 0;
    private int lostLandmarkCount_ = 0;
    private int suspendedLandmarkCount_ = 0;
    private int maskedLandmarkCount_ = 0;
    private IntPtr landmarkBuffer_ = IntPtr.Zero;
    private IntPtr initPointBuffer_ = IntPtr.Zero;
    private bool showStatus_ = true;
    private bool useTriangulateMasks_ = false;
    private int videoImageSizeSelected_ = -1;
    private int stillImageSizeSelected_ = -1;
    private int focusModeSelected_ = -1;
    private int flashModeSelected_ = -1;
    private int exposureModeSelected_ = -1;
    private int whiteBalanceModeSelected_ = -1;
    private int sceneModeSelected_ = -1;
    private int videoImageFpsRangeSelected_ = -1;
    private smartar.DenseMapMode denseMapMode_ = smartar.DenseMapMode.DENSE_MAP_DISABLE;
    private smartar.SearchPolicy searchPolicy_ = smartar.SearchPolicy.SEARCH_POLICY_PRECISIVE;
    private const int SUPPORTED_NUM_OF_IMAGE_SIZE_MAX = 32;
    private const int FPS_RANGE_STRING_LENGTH = 32;
    private const int RETURN_ARRAY_MAX_SIZE = 32;
    private bool useSensorDevice_;
    private bool fixSceneMap_;
    private MenuMode MenuMode_;
    private enum MenuMode
    {
        Root,
        Scene_mappings,
        Target_trackings,
        Camera_settings,
        Miscs,
        SupportedVideoImageSize,
        SupportedStillImageSize,
        SetFocusMode,
        SetFlashMode,
        SetExposureMode,
        SetWhiteBalanceMode,
        SetSceneMode,
        SetFpsRange,
    };

    // For capture still image
    private string m_CaptureImageName;
    private string m_CaptureImagePath;
    private bool m_ShowGUI = true;
    private bool m_DoCapture = false;

    //For camera device status
    string[] FocusModeArray =
    {
        "MANUAL",
        "CONTINUOUS_AUTO_PICTURE",
        "CONTINUOUS_AUTO_VIDEO",
        "EDOF",
        "FIXED",
        "INFINITY",
        "MACRO"
    };
    string[] FlashModeArray =
    {
        "AUTO",
        "OFF",
        "ON",
        "RED_EYE",
        "TORCH"
    };
    string[] ExposureModeArray =
    {
        "MANUAL",
        "CONTINUOUS_AUTO"
    };
    string[] WhiteBalanceModeArray =
    {
        "CONTINUOUS_AUTO",
        "CLOUDY_DAYLIGHT",
        "DAYLIGHT",
        "FLUORESCENT",
        "INCANDESCENT",
        "SHADE",
        "TWILIGHT",
        "WARM_FLUORESCENT",
        "MANUAL"
    };
    string[] SceneModeArray =
    {
        "ACTION",
        "AUTO",
        "BARCODE",
        "BEACH",
        "CANDLELIGHT",
        "FIREWORKS",
        "LANDSCAPE",
        "NIGHT",
        "NIGHT_PORTRAIT",
        "PARTY",
        "PORTRAIT",
        "SNOW",
        "SPORTS",
        "STEADYPHOTO",
        "SUNSET",
        "THEATRE"
    };
    private smartar.Triangle2[] TRIANGULATE_MASK_NULL = new smartar.Triangle2[] {};
    private smartar.Triangle2[] TRIANGULATE_MASK = new smartar.Triangle2[]
    {
        new smartar.Triangle2{
            p0_ = new smartar.Vector2{x_ = 0.5f, y_ = 0.0f},
            p1_ = new smartar.Vector2{x_ = 1.0f, y_ = 0.0f},
            p2_ = new smartar.Vector2{x_ = 1.0f, y_ = 0.5f}
        },
        new smartar.Triangle2{
            p0_ = new smartar.Vector2{x_ = 0.0f, y_ = 0.5f},
            p1_ = new smartar.Vector2{x_ = 0.0f, y_ = 1.0f},
            p2_ = new smartar.Vector2{x_ = 0.5f, y_ = 1.0f}
        },
        new smartar.Triangle2{
            p0_ = new smartar.Vector2{x_ = 0.5f, y_ = 1.0f},
            p1_ = new smartar.Vector2{x_ = 1.0f, y_ = 0.5f},
            p2_ = new smartar.Vector2{x_ = 1.0f, y_ = 1.0f}
        },
        new smartar.Triangle2{
            p0_ = new smartar.Vector2{x_ = 0.0f, y_ = 0.0f},
            p1_ = new smartar.Vector2{x_ = 0.5f, y_ = 0.0f},
            p2_ = new smartar.Vector2{x_ = 0.0f, y_ = 0.5f}
        },
    };

    protected override void Awake()
    {
        base.Awake();

        // Init fields for LandmarkDrawer
        landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
        initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);
        effectors_ = FindObjectsOfType<SmartAREffector>();
    }

    private void DoEnable()
    {
        initPreferences();
        enabled_ = true;
    }

    protected override void Update()
    {
        if (smartARController == null) { return; }

        base.Update();

        if (smartARController.smart_.isConstructorFailed()) { return; }

        if (smartARController.enabled_ && !enabled_)
        {
            DoEnable();
        }

        drawObjects();

        if (showStatus_)
        {
            updateStatus();
        }

        if (useTriangulateMasks_)
        {
            smartARController.triangulateMasks_ = TRIANGULATE_MASK;
        }
        else
        {
            smartARController.triangulateMasks_ = TRIANGULATE_MASK_NULL;
        }
    }

    private Vector2 scrollPosition;

    protected override void OnGUI()
    {
        base.OnGUI();

        if (smartARController.smart_.isConstructorFailed()) { return; }

        if (!m_ShowGUI)
        {
            m_DoCapture = true;
            return;
        }

        if (showStatus_)
        {
            GUI.Label(new Rect(0, 0, 1000, 1000), labelText);
        }
        
        int landmarkWidthUnit;
        int landmarkCount = trackedLandmarkCount_ + lostLandmarkCount_ + suspendedLandmarkCount_ + maskedLandmarkCount_;
        if (landmarkCount == 0)
        {
            landmarkWidthUnit = 0;
        }
        else
        {
            landmarkWidthUnit = Screen.width / landmarkCount / 2;
        }
        
        if (showStatus_)
        {
            int landmarkPositionX = 0;
            int landmarkLength = landmarkWidthUnit * trackedLandmarkCount_;
            GUIStyle BoxStyle = new GUIStyle(mySkin.box);
            BoxStyle.normal.background = green;
            GUI.Box(new Rect(landmarkPositionX, 350, landmarkLength, 20), "", BoxStyle);
            landmarkPositionX += landmarkLength;
            landmarkLength = landmarkWidthUnit * lostLandmarkCount_;
            BoxStyle.normal.background = red;
            GUI.Box(new Rect(landmarkPositionX, 350, landmarkLength, 20), "", BoxStyle);
            landmarkPositionX += landmarkLength;
            landmarkLength = landmarkWidthUnit * suspendedLandmarkCount_;
            BoxStyle.normal.background = cyan;
            GUI.Box(new Rect(landmarkPositionX, 350, landmarkLength, 20), "", BoxStyle);
            landmarkPositionX += landmarkLength;
            landmarkLength = landmarkWidthUnit * maskedLandmarkCount_;
            BoxStyle.normal.background = yerrow;
            GUI.Box(new Rect(landmarkPositionX, 350, landmarkLength, 20), "", BoxStyle);
        }
        
        if (GUI.Button(new Rect(0, Screen.height - 100, 200, 100), "Reset"))
        {
			smartARController.restartController();
			cameraFrameFps_ = 0.0f;
			lastCameraFrameCount_ = 0;
			for (int i = 0; i < 2; ++i)
			{
				lastRecogCount_ [i] = 0;
				lastRecogTime_ [i] = 0;
				recognizerFps_ [i] = 0.0f;
			}
		}
        
        if (GUI.Button(new Rect(Screen.width - 200, Screen.height - 100, 200, 100), "Menu"))
        {
            MenuMode_ = MenuMode.Root;
            isMenuShow = !isMenuShow;
        }
        
        if (isMenuShow)
        {
            menuButtonStyle = new GUIStyle(mySkin.button);
            menuButtonStyle.overflow = new RectOffset(10, 10, 15, 15);
            menuButtonStyle.margin = new RectOffset(0, 0, 50, 50);
            
            selectedMenuButtonStyle = new GUIStyle(menuButtonStyle);
            selectedMenuButtonStyle.normal.textColor = Color.red;
            
            cannotSelectMenuButtonStyle = new GUIStyle(menuButtonStyle);
            cannotSelectMenuButtonStyle.normal.textColor = Color.gray;
            
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(500), GUILayout.Height(500));
            switch (MenuMode_)
            {
                case MenuMode.Root:
                    handleRootMenu();
                    break;
                case MenuMode.Scene_mappings:
                    handleSceneMappingMenu();
                    break;
                case MenuMode.Target_trackings:
                    handleTargetTrackingMenu();
                    break;
                case MenuMode.Camera_settings:
                    handleCameraSettingMenu();
                    break;
                case MenuMode.Miscs:
                    handleMiscs();
                    break;
                case MenuMode.SupportedVideoImageSize:
                    showSupportedVideoImageSize();
                    break;
                case MenuMode.SupportedStillImageSize:
                    showSupportedStillImageSize();
                    break;
                case MenuMode.SetFocusMode:
                    showSetFocusMode();
                    break;
                case MenuMode.SetFlashMode:
                    showSetFlashMode();
                    break;
                case MenuMode.SetExposureMode:
                    showSetExposureMode();
                    break;
                case MenuMode.SetWhiteBalanceMode:
                    showSetWhiteBalanceMode();
                    break;
                case MenuMode.SetSceneMode:
                    showSetSceneMode();
                    break;
                case MenuMode.SetFpsRange:
                    showSetFpsRange();
                    break;
                default:
                    break;
            }
            GUILayout.EndScrollView();
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }

    void OnPostRender()
    {
        if (smartARController.smart_.isConstructorFailed()) { return; }

        // Capture still image
        if (!m_ShowGUI && m_DoCapture)
        {
            if (string.IsNullOrEmpty(m_CaptureImageName)) { return; }

            onPreCaptureImage();
            Application.CaptureScreenshot(m_CaptureImageName);
            m_CaptureImageName = null;
            StartCoroutine(waitUntilFinishedWriting(() => {
                onPostCaptureImage();
            }));
        }
    }

    private void handleRootMenu()
    {
        if (smartARController.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING)
        {
            if (GUILayout.Button("Scene mappings", menuButtonStyle))
            {
                MenuMode_ = MenuMode.Scene_mappings;
            }
        }
        else
        {
            if (GUILayout.Button("Scene mappings", cannotSelectMenuButtonStyle))
            {
                //DO nothing
            }
        }
        if (smartARController.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING)
        {
            if (GUILayout.Button("Target trackings", menuButtonStyle))
            {
                MenuMode_ = MenuMode.Target_trackings;
            }
        }
        else
        {
            if (GUILayout.Button("Target trackings", cannotSelectMenuButtonStyle))
            {
                //DO nothing
            }
        }
        if (GUILayout.Button("Camera settings", menuButtonStyle))
        {
            MenuMode_ = MenuMode.Camera_settings;
        }
        if (GUILayout.Button("Misc", menuButtonStyle))
        {
            MenuMode_ = MenuMode.Miscs;
        }
    }

    private void handleSceneMappingMenu()
    {
        if (GUILayout.Button("Save scene map", menuButtonStyle))
        {
            var sceneMapFilePath = Application.persistentDataPath + SCENE_MAP_FILE_NAME;
            var fileStreamOut = new smartar.FileStreamOut(smartARController.smart_, sceneMapFilePath);
            smartARController.recognizer_.SaveSceneMap(fileStreamOut);
        }
        if (GUILayout.Button("Load scene map", menuButtonStyle))
        {
            var sceneMapFilePath = Application.persistentDataPath + SCENE_MAP_FILE_NAME;
            smartARController.loadSceneMap(sceneMapFilePath);
        }
        if (GUILayout.Button("FixSceneMap ON", menuButtonStyle))
        {
            fixSceneMap_ = true;
            smartARController.recognizer_.FixSceneMap(fixSceneMap_);
        }
        if (GUILayout.Button("FixSceneMap OFF", menuButtonStyle))
        {
            fixSceneMap_ = false;
            smartARController.recognizer_.FixSceneMap(fixSceneMap_);
        }
        if (GUILayout.Button("Change dense map mode", menuButtonStyle))
        {
            denseMapMode_ = smartARController.recognizerSettings_.denseMapMode == smartar.DenseMapMode.DENSE_MAP_DISABLE
                          ? smartar.DenseMapMode.DENSE_MAP_SEMI_DENSE
                          : smartar.DenseMapMode.DENSE_MAP_DISABLE;
            smartARController.recognizer_.SetDenseMapMode(denseMapMode_);
            smartARController.recognizerSettings_.denseMapMode = denseMapMode_;
        }
        if (GUILayout.Button("Remove lost landmarks ON", menuButtonStyle))
        {
            removeLostLandmarks_ = true;
        }
        if (GUILayout.Button("Remove lost landmarks OFF", menuButtonStyle))
        {
            removeLostLandmarks_ = false;
        }
        if (GUILayout.Button("TriangulateMasks ON", menuButtonStyle))
        {
            useTriangulateMasks_ = true;
        }
        if (GUILayout.Button("TriangulateMasks OFF", menuButtonStyle))
        {
            useTriangulateMasks_ = false;
        }
    }

    private void handleTargetTrackingMenu()
    {
        if (GUILayout.Button("Change search policy", menuButtonStyle))
        {
            searchPolicy_ = searchPolicy_ == smartar.SearchPolicy.SEARCH_POLICY_PRECISIVE
                          ? smartar.SearchPolicy.SEARCH_POLICY_FAST
                          : smartar.SearchPolicy.SEARCH_POLICY_PRECISIVE;
            smartARController.recognizer_.SetSearchPolicy(searchPolicy_);
        }
    }

    private void handleCameraSettingMenu()
    {
        if (GUILayout.Button("Set FocusMode", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetFocusMode;
        }
        if (GUILayout.Button("Set FlashMode", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetFlashMode;
        }
        if (GUILayout.Button("Set fps range", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetFpsRange;
        }
        if (GUILayout.Button("Set SceneMode", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetSceneMode;
        }
        if (GUILayout.Button("Set WhiteBalanceMode", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetWhiteBalanceMode;
        }
        if (GUILayout.Button("Set ExposureMode", menuButtonStyle))
        {
            MenuMode_ = MenuMode.SetExposureMode;
        }
    }

    private void handleMiscs()
    {
        if (GUILayout.Button("Sensor device ON", menuButtonStyle))
        {
            smartARController.sensorDevice_.Start();
            useSensorDevice_ = true;
        }
        if (GUILayout.Button("Sensor device OFF", menuButtonStyle))
        {
            smartARController.sensorDevice_.Stop();
            useSensorDevice_ = false;
        }
        if (GUILayout.Button("ForceLocalize", menuButtonStyle))
        {
            smartARController.cameraDevice_.Stop();
            smartARController.recognizer_.ForceLocalize();
            smartARController.cameraDevice_.Start();
        }
        if (GUILayout.Button("Show or hide Status", menuButtonStyle))
        {
            showStatus_ = !showStatus_;
        }
        if (GUILayout.Button("Capture image", menuButtonStyle))
        {
//            smartARController.cameraDevice_.CaptureStillImage();
            m_CaptureImageName = string.Format("capture_image_{0}.png", DateTime.Now.ToString("d-MM-yyyy-HH-mm-ss-f"));
            m_CaptureImagePath = createCaptureImagePath(m_CaptureImageName);
            m_ShowGUI = false;
        }
        if (GUILayout.Button("Save preference", menuButtonStyle))
        {
            savePreferences();
        }
        if (GUILayout.Button("Reset all settings", menuButtonStyle))
        {
            resetAllSettings();
        }
    }

    private void showSupportedVideoImageSize()
    {
        smartar.Size[] sizes = new smartar.Size[SUPPORTED_NUM_OF_IMAGE_SIZE_MAX];
        int numOfVideoImageSize = smartARController.cameraDevice_.GetSupportedVideoImageSize(sizes);

        if (videoImageSizeSelected_ == -1)
        {
            videoImageSizeSelected_ = getNearestVideoImageSize(sizes, numOfVideoImageSize, VGA);
        }

        GUIStyle style;
        for (int i = 0; i < numOfVideoImageSize; i++)
        {
            if (sizes [i].width_ == 0 && sizes [i].height_ == 0) break;
            style = (i == videoImageSizeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(sizes [i].width_ + " * " + sizes [i].height_, style))
            {
                smartARController.cameraDevice_.Stop();
                smartARController.cameraDevice_.SetVideoImageSize(sizes [i].width_, sizes [i].height_);
                videoImageSizeSelected_ = i;
                smartARController.cameraDevice_.Start();
            }
        }
    }

    private int getNearestVideoImageSize(smartar.Size[] sizeBuffer, int numOfVideoImageSize, smartar.Size targetSize)
    {
        int curNearestIndex = 0;
        float curDiff = 0;
        for (int i = 0; i < numOfVideoImageSize; i++)
        {
            float diffX = (float)Math.Abs(sizeBuffer [i].width_ - targetSize.width_)
                          / Math.Min(sizeBuffer [i].width_, targetSize.width_);
            float diffY = (float)Math.Abs(sizeBuffer [i].height_ - targetSize.height_)
                          / Math.Min(sizeBuffer [i].height_, targetSize.height_);
            float diff = diffX + diffY;
            bool isAlmostSame = Math.Abs(diff - curDiff) < 10e-6f;
            bool isCurrentBigger = sizeBuffer [i].width_ * sizeBuffer [i].height_
                                   < sizeBuffer [curNearestIndex].width_ * sizeBuffer [curNearestIndex].height_;
            if (i == 0 || diff < curDiff || (isAlmostSame && isCurrentBigger))
            {
                curNearestIndex = i;
                curDiff = diff;
            }
        }

        return curNearestIndex;
    }

    private void showSupportedStillImageSize()
    {
        smartar.Size[] sizes2 = new smartar.Size[SUPPORTED_NUM_OF_IMAGE_SIZE_MAX];
        int numOfStillImageSize = smartARController.cameraDevice_.GetSupportedStillImageSize(sizes2);

        if (stillImageSizeSelected_ == -1)
        {
            stillImageSizeSelected_ = getBiggestStillImageSize(sizes2, numOfStillImageSize);
        }

        GUIStyle style;
        for (int i = 0; i < numOfStillImageSize; i++)
        {
            if (sizes2 [i].width_ == 0 && sizes2 [i].height_ == 0) break;
            style = (i == stillImageSizeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(sizes2 [i].width_ + " * " + sizes2 [i].height_, style))
            {
                smartARController.cameraDevice_.SetStillImageSize(sizes2 [i].width_, sizes2 [i].height_);
                stillImageSizeSelected_ = i;
            }
        }
    }

    private int getBiggestStillImageSize(smartar.Size[] sizeBuffer, int numOfStillImageSize)
    {
        int curBiggestIndex = 0;
        float biggestMeasureSize = 0;
        for (int i = 0; i < numOfStillImageSize; i++)
        {
            float curMeasureSize = sizeBuffer [i].width_ * sizeBuffer [i].height_;
            if (curMeasureSize > biggestMeasureSize)
            {
                curBiggestIndex = i;
                biggestMeasureSize = curMeasureSize;
            }
        }

        return curBiggestIndex;
    }

    private void showSetFocusMode()
    {
        smartar.FocusMode[] focusmodes = new smartar.FocusMode[FocusModeArray.Length];
        int numOfFocusMode = smartARController.cameraDevice_.GetSupportedFocusMode(focusmodes);

        if (focusModeSelected_ == -1)
        {
            for (int i = 0; i < numOfFocusMode; i++)
            {
                if (focusmodes [i] == DEFAULT_FOCUS_MODE_FIRST)
                {
                    focusModeSelected_ = i;
                }
            }

            if (focusModeSelected_ == -1)
            {
                for (int i = 0; i < numOfFocusMode; i++)
                {
                    if (focusmodes [i] == DEFAULT_FOCUS_MODE_SECOND)
                    {
                        focusModeSelected_ = i;
                    }
                }
            }

            if (focusModeSelected_ == -1)
            {
                for (int i = 0; i < numOfFocusMode; i++)
                {
                    if (focusmodes [i] == DEFAULT_FOCUS_MODE_THIRD)
                    {
                        focusModeSelected_ = i;
                    }
                }
            }

            if (focusModeSelected_ == -1)
            {
                focusModeSelected_ = 0;
            }
        }

        GUIStyle style;
        for (int i = 0; i < numOfFocusMode; i++)
        {
            style = (i == focusModeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(FocusModeArray [(int)focusmodes [i]], style))
            {
                smartARController.cameraDevice_.SetFocusMode(focusmodes [i]);
                focusModeSelected_ = i;
            }
        }
    }

    private void showSetFlashMode()
    {
        smartar.FlashMode[] flashModes = new smartar.FlashMode[FlashModeArray.Length];
        int numOfFlashMode = smartARController.cameraDevice_.GetSupportedFlashMode(flashModes);

        if (flashModeSelected_ == -1)
        {
            for (int i = 0; i < numOfFlashMode; i++)
            {
                if (flashModes [i] == DEFAULT_FLASH_MODE)
                {
                    flashModeSelected_ = i;
                }
            }
            if (flashModeSelected_ == -1)
            {
                flashModeSelected_ = 0;
            }
        }

        GUIStyle style;
        for (int i = 0; i < numOfFlashMode; i++)
        {
            style = (i == flashModeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(FlashModeArray [(int)flashModes [i]], style))
            {
                smartARController.cameraDevice_.SetFlashMode(flashModes [i]);
                flashModeSelected_ = i;
            }
        }
    }

    private void showSetExposureMode()
    {
        smartar.ExposureMode[] exposureModes = new smartar.ExposureMode[ExposureModeArray.Length];
        int numOfExposureMode = smartARController.cameraDevice_.GetSupportedExposureMode(exposureModes);

        if (exposureModeSelected_ == -1)
        {
            for (int i = 0; i < numOfExposureMode; i++)
            {
                if (exposureModes [i] == DEFAULT_EXPOSURE_MODE)
                {
                    exposureModeSelected_ = i;
                }
            }
            if (exposureModeSelected_ == -1)
            {
                exposureModeSelected_ = 0;
            }
        }

        GUIStyle style;
        for (int i = 0; i < numOfExposureMode; i++)
        {
            style = (i == exposureModeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(ExposureModeArray [(int)exposureModes [i]], style))
            {
                smartARController.cameraDevice_.SetExposureMode(exposureModes [i]);
                exposureModeSelected_ = i;
            }
        }
    }

    private void showSetWhiteBalanceMode()
    {
        smartar.WhiteBalanceMode[] whiteBalanceModes = new smartar.WhiteBalanceMode[WhiteBalanceModeArray.Length];
        int numOfWhiteBalanceMode = smartARController.cameraDevice_.GetSupportedWhiteBalanceMode(whiteBalanceModes);

        if (whiteBalanceModeSelected_ == -1)
        {
            for (int i = 0; i < numOfWhiteBalanceMode; i++)
            {
                if (whiteBalanceModes [i] == DEFAULT_WHITE_BALANCE_MODE)
                {
                    whiteBalanceModeSelected_ = i;
                }
            }
            if (whiteBalanceModeSelected_ == -1)
            {
                whiteBalanceModeSelected_ = 0;
            }
        }

        GUIStyle style;
        for (int i = 0; i < numOfWhiteBalanceMode; i++)
        {
            style = (i == whiteBalanceModeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(WhiteBalanceModeArray [(int)whiteBalanceModes [i]], style))
            {
                smartARController.cameraDevice_.SetWhiteBalanceMode(whiteBalanceModes [i]);
                whiteBalanceModeSelected_ = i;
            }
        }
    }

    private void showSetSceneMode()
    {
        smartar.SceneMode[] sceneModes = new smartar.SceneMode[SceneModeArray.Length];
        int numOfSceneMode = smartARController.cameraDevice_.GetSupportedSceneMode(sceneModes);

        if (sceneModeSelected_ == -1)
        {
            for (int i = 0; i < numOfSceneMode; i++)
            {
                if (sceneModes [i] == DEFAULT_SCENE_MODE)
                {
                    sceneModeSelected_ = i;
                }
            }
            if (sceneModeSelected_ == -1)
            {
                sceneModeSelected_ = 0;
            }
        }

        GUIStyle style;
        for (int i = 0; i < numOfSceneMode; i++)
        {
            style = (i == sceneModeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(SceneModeArray [(int)sceneModes [i]], style))
            {
                smartARController.cameraDevice_.SetSceneMode(sceneModes [i]);
                sceneModeSelected_ = i;
            }
        }
    }

    private void showSetFpsRange()
    {
        smartar.CameraFpsRange[] CameraFpsRanges = new smartar.CameraFpsRange[FPS_RANGE_STRING_LENGTH];
        int numOfFpsRanges = smartARController.cameraDevice_.GetSupportedVideoImageFpsRange(CameraFpsRanges);

        if (videoImageFpsRangeSelected_ == -1)
        {
            videoImageFpsRangeSelected_ = getNearestVideoImageFpsRange(CameraFpsRanges, numOfFpsRanges, BEST_FPS);
        }

        GUIStyle style;
        for (int i = 0; i < numOfFpsRanges; i++)
        {
            style = (i == videoImageFpsRangeSelected_) ? selectedMenuButtonStyle : menuButtonStyle;
            if (GUILayout.Button(CameraFpsRanges [i].min_ + "-" + CameraFpsRanges [i].max_, style))
            {
                smartARController.cameraDevice_.SetVideoImageFpsRange(CameraFpsRanges [i].min_, CameraFpsRanges [i].max_);
                videoImageFpsRangeSelected_ = i;
            }
        }
    }

    private int getNearestVideoImageFpsRange(smartar.CameraFpsRange[] cameraFpsRanges, int numOfFpsRanges, float bestFps)
    {
        int curNearestIndex = 0;
        float curMaxDiff = 0;
        for (int i = 0; i < numOfFpsRanges; i++)
        {
            float maxDiff = Math.Abs(bestFps - cameraFpsRanges [i].max_);
            bool isAlmostSame = Math.Abs(maxDiff - curMaxDiff) < 10e-6f;
            bool isCurrentMinBigger = cameraFpsRanges [curNearestIndex].min_ < cameraFpsRanges [i].min_;
            if (i == 0 || maxDiff < curMaxDiff || (isAlmostSame && isCurrentMinBigger))
            {
                curNearestIndex = i;
                curMaxDiff = maxDiff;
            }
        }

        return curNearestIndex;
    }

    void Start()
    {
    }

    private void drawObjects()
    {
        trackedLandmarkCount_ = 0;
        lostLandmarkCount_ = 0;
        suspendedLandmarkCount_ = 0;
        maskedLandmarkCount_ = 0;

        bool isMultiRecognized = false;

        if (smartARController.targets_ == null) { return; }

        foreach (var target in smartARController.targets_)
        {
            foreach (var effector in effectors_)
            {
                if (effector.result_.isRecognized_)
                {
                    if (effector.result_.target_ == target.self_ && !smartARController.isLoadSceneMap_)
                    {
                        var size = new smartar.Vector2();
                        target.GetPhysicalSize(out size);
                        var child = effector.transform.FindChild("Frame");
                        child.localScale = new Vector3(size.x_ * effector.nearClipPlane_, 150f, size.y_ * effector.nearClipPlane_);
                    }
                    else if (effector.targetID == null)
                    {
                        var child = effector.transform.FindChild("Frame");
                        child.localScale = new Vector3(300f, 150f, 300f);
                    }
                    countLandmarks(effector.result_);
                }

                switch (smartARController.recognizerSettings_.recognitionMode)
                {
                case smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING:
                    if (effector.result_.sceneMappingState_ != smartar.SceneMappingState.SCENE_MAPPING_STATE_IDLE)
                    {
                        sceneMappingStateresult_ = effector.result_.sceneMappingState_;
                        isMultiRecognized = true;
                    }
                    break;

                case smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING:
                    if (effector.result_.targetTrackingState_ != smartar.TargetTrackingState.TARGET_TRACKING_STATE_IDLE)
                    {
                        targetTrackingStateResult_ = effector.result_.targetTrackingState_;
                        isMultiRecognized = true;
                    }
                    break;
                }
            }
        }

        if (!isMultiRecognized)
        {
            sceneMappingStateresult_ = smartar.SceneMappingState.SCENE_MAPPING_STATE_IDLE;
            targetTrackingStateResult_ = smartar.TargetTrackingState.TARGET_TRACKING_STATE_IDLE;
        }
    }

    private void countLandmarks(smartar.RecognitionResult result)
    {
        trackedLandmarkCount_ = 0;
        lostLandmarkCount_ = 0;
        suspendedLandmarkCount_ = 0;
        maskedLandmarkCount_ = 0;

        smartar.Landmark[] landmarkArray = new smartar.Landmark[result.numLandmarks_];
        IntPtr landmarkPointer = result.landmarks_;
        for (int i = 0; i < result.numLandmarks_; i++)
        {
            landmarkArray [i] = new smartar.Landmark();
            landmarkArray [i] = (smartar.Landmark)Marshal.PtrToStructure(landmarkPointer, typeof(smartar.Landmark));
            landmarkPointer = new IntPtr(landmarkPointer.ToInt64() + (Int64)Marshal.SizeOf(landmarkArray[i]));
            switch (landmarkArray [i].state_)
            {
            case smartar.LandmarkState.LANDMARK_STATE_TRACKED:
                //Tracked Landmark:GLEEN
                trackedLandmarkCount_++;
                break;
            case smartar.LandmarkState.LANDMARK_STATE_LOST:
                //Lost landmark:RED
                lostLandmarkCount_++;
                //If removeLostLandmarks_ is true, remove lost landmarks.
                if (removeLostLandmarks_)
                {
                    smartARController.recognizer_.RemoveLandmark(landmarkArray [i]);
                }
                break;
            case smartar.LandmarkState.LANDMARK_STATE_SUSPENDED:
                //Suspended landmark:CYAN
                suspendedLandmarkCount_++;
                break;
            case smartar.LandmarkState.LANDMARK_STATE_MASKED:
                //Masked landmark:YELLOW
                maskedLandmarkCount_++;
                break;
            default:
                break;
            }
        }
    }

    System.Text.StringBuilder stringBuilder_ = new System.Text.StringBuilder();
    private void updateStatus()
    {
        ++drawFrameCount_;
        float time = Time.realtimeSinceStartup;
        float timeDiff = time - lastUpdateTime_;
        if (timeDiff >= UPDATE_INTERVAL)
        {
            lastUpdateTime_ = time;

            cameraFrameFps_ = (smartARController.cameraFrameCount_ - lastCameraFrameCount_) / timeDiff;
            lastCameraFrameCount_ = smartARController.cameraFrameCount_;

            ulong[] curRecogCount = smartARController.recogCount_;
            ulong[] curRecogTime = smartARController.recogTime_;
            for (int i = 0; i < curRecogCount.Length; ++i)
            {
                ulong deltaRecogCount = curRecogCount [i] - lastRecogCount_ [i];
                ulong deltaRecogTime = curRecogTime [i] - lastRecogTime_ [i];

                recognizerFps_ [i] = deltaRecogCount / timeDiff;
                if (deltaRecogCount > 0)
                {
                    recognizerTime_ [i] = deltaRecogTime / (float)deltaRecogCount;
                }
                else
                {
                    recognizerTime_ [i] = -1;
                }
                lastRecogCount_ [i] = curRecogCount [i];
                lastRecogTime_ [i] = curRecogTime [i];
            }

            drawFrameFps_ = (drawFrameCount_ - lastDrawFrameCount_) / timeDiff;
            if (drawFrameCount_ - lastDrawFrameCount_ > 0)
            {
                drawFrameTime_ = (smartARController.drawTime_ - lastDrawTime_) / (float)(drawFrameCount_ - lastDrawFrameCount_);
            }
            else
            {
                drawFrameTime_ = -1;
            }
            lastDrawFrameCount_ = drawFrameCount_;
            lastDrawTime_ = smartARController.drawTime_;

            updateLabelText();
        }

        labelText = stringBuilder_.ToString();
    }

    private void updateLabelText()
    {
        //Draw state label
        stringBuilder_.Length = 0;
        stringBuilder_.Append("CameraFrame fps: ");
        stringBuilder_.Append((int)cameraFrameFps_).Append(".").Append(getF2(cameraFrameFps_, 1)).Append(getF2(cameraFrameFps_, 2));

        stringBuilder_.Append("\n");
        for (int i = 0; i < smartARController.recogCount_.Length; ++i)
        {
            stringBuilder_.Append("Recognizer").Append(i).Append(" fps: ");
            stringBuilder_.Append((int)recognizerFps_ [i]).Append(".").Append(getF2(recognizerFps_ [i], 1)).Append(getF2(recognizerFps_ [i], 2));
            stringBuilder_.Append("[");
            if (recognizerTime_ [i] != -1)
            {
                stringBuilder_.Append((int)recognizerTime_ [i]).Append(".").Append(getF2(recognizerTime_ [i], 1)).Append(getF2(recognizerTime_ [i], 2));
            }
            else
            {
                stringBuilder_.Append("-");
            }
            stringBuilder_.Append("ms] ");
        }
        stringBuilder_.Append("\nDrawframe fps: ");
        stringBuilder_.Append((int)drawFrameFps_).Append(".").Append(getF2(drawFrameFps_, 1)).Append(getF2(drawFrameFps_, 2));
        stringBuilder_.Append("[");
        if (drawFrameTime_ != -1)
        {
            stringBuilder_.Append((int)drawFrameTime_).Append(".").Append(getF2(drawFrameTime_, 1)).Append(getF2(drawFrameTime_, 2));
        }
        else
        {
            stringBuilder_.Append("-");
        }
        stringBuilder_.Append("ms] ");

        stringBuilder_.Append("\nRecognition Mode:");
        switch (smartARController.recognizerSettings_.recognitionMode)
        {
        case smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING:
            stringBuilder_.Append("SCENE_MAPPING");
            stringBuilder_.Append("\nState:");
            switch (sceneMappingStateresult_)
            {
            case smartar.SceneMappingState.SCENE_MAPPING_STATE_IDLE:
                stringBuilder_.Append("IDLE");
                break;
            case smartar.SceneMappingState.SCENE_MAPPING_STATE_LOCALIZE:
                stringBuilder_.Append("LOCALIZE");
                break;
            case smartar.SceneMappingState.SCENE_MAPPING_STATE_LOCALIZE_IMPOSSIBLE:
                stringBuilder_.Append("IMPOSSIBLE");
                break;
            case smartar.SceneMappingState.SCENE_MAPPING_STATE_SEARCH:
                stringBuilder_.Append("SEARCH");
                break;
            case smartar.SceneMappingState.SCENE_MAPPING_STATE_TRACKING:
                stringBuilder_.Append("TRACKING");
                break;
            default:
                break;
            }
            stringBuilder_.Append("\nSceneMapping initMode:");
            switch (smartARController.recognizerSettings_.sceneMappingInitMode)
            {
            case smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET:
                stringBuilder_.Append("TARGET");
                break;
            case smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_HFG:
                stringBuilder_.Append("HFG");
                break;
            case smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_VFG:
                stringBuilder_.Append("VFG");
                break;
            case smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_SFM:
                stringBuilder_.Append("SFM");
                break;
            case smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_DRY_RUN:
                stringBuilder_.Append("DRY_RUN");
                break;
            default:
                stringBuilder_.Append("ERROR_INIT_MODE");
                break;
            }
            break;
        case smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING:
            stringBuilder_.Append("TARGET_TRACKING");
            stringBuilder_.Append("\nState:");
            switch (targetTrackingStateResult_)
            {
            case smartar.TargetTrackingState.TARGET_TRACKING_STATE_IDLE:
                stringBuilder_.Append("IDLE");
                break;
            case smartar.TargetTrackingState.TARGET_TRACKING_STATE_SEARCH:
                stringBuilder_.Append("SEARCH");
                break;
            case smartar.TargetTrackingState.TARGET_TRACKING_STATE_TRACKING:
                stringBuilder_.Append("TRACKING");
                break;
            default:
                break;
            }
            stringBuilder_.Append("\nSceneMapping initMode:NOT_SCENE_MAPPING");
            break;
        default:
            stringBuilder_.Append("ERROR_MODE");
            stringBuilder_.Append("\nState:ERROR_STATE");
            stringBuilder_.Append("\nSceneMapping initMode:ERROR_INIT_MODE");
            break;
        }

        stringBuilder_.Append("\nSearch policy:");
        switch (searchPolicy_)
        {
        case smartar.SearchPolicy.SEARCH_POLICY_FAST:
            stringBuilder_.Append("FAST");
            break;
        case smartar.SearchPolicy.SEARCH_POLICY_PRECISIVE:
            stringBuilder_.Append("PRECISIVE");
            break;
        default:
            break;
        }

        stringBuilder_.Append("\nDense map mode:");
        switch (smartARController.recognizerSettings_.denseMapMode)
        {
        case smartar.DenseMapMode.DENSE_MAP_DISABLE:
            stringBuilder_.Append("DISABLE");
            break;
        case smartar.DenseMapMode.DENSE_MAP_SEMI_DENSE:
            stringBuilder_.Append("SEMI_DENSE");
            break;
        default:
            break;
        }

        stringBuilder_.Append("\nLandmarks:");
        stringBuilder_.Append("\nTracked:").Append(trackedLandmarkCount_);
        stringBuilder_.Append(" Lost:").Append(lostLandmarkCount_);
        stringBuilder_.Append(" Suspended:").Append(suspendedLandmarkCount_);
        stringBuilder_.Append(" Masked:").Append(maskedLandmarkCount_);
    }

    private int getF2(float num, int digit)
    {
        if (digit == 1)
        {
            return (int)(num * 10) - ((int)num) * 10;
        }
        else if (digit == 2)
        {
            num = num * 10;
            return (int)(num * 10) - ((int)num) * 10;
        }
        return 0;
    }

    private void initPreferences()
    {
        // Focus mode
        focusModeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_FOCUS_MODE, -1);
        if (focusModeSelected_ != -1)
        {
            smartar.FocusMode[] focusmodes = new smartar.FocusMode[FocusModeArray.Length];
            smartARController.cameraDevice_.GetSupportedFocusMode(focusmodes);
            smartARController.cameraDevice_.SetFocusMode(focusmodes [focusModeSelected_]);
        }

        // Flash mode
        flashModeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_FLASH_MODE, -1);
        if (flashModeSelected_ != -1)
        {
            smartar.FlashMode[] flashModes = new smartar.FlashMode[FlashModeArray.Length];
            smartARController.cameraDevice_.GetSupportedFlashMode(flashModes);
            smartARController.cameraDevice_.SetFlashMode(flashModes [flashModeSelected_]);
        }

        // Scene mode
        sceneModeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_SCENE_MODE, -1);
        if (sceneModeSelected_ != -1)
        {
            smartar.SceneMode[] sceneModes = new smartar.SceneMode[SceneModeArray.Length];
            smartARController.cameraDevice_.GetSupportedSceneMode(sceneModes);
            smartARController.cameraDevice_.SetSceneMode(sceneModes [sceneModeSelected_]);
        }

        // White balance modep
        whiteBalanceModeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_WHITE_BALANCE_MODE, -1);
        if (whiteBalanceModeSelected_ != -1)
        {
            smartar.WhiteBalanceMode[] whiteBalanceModes = new smartar.WhiteBalanceMode[WhiteBalanceModeArray.Length];
            smartARController.cameraDevice_.GetSupportedWhiteBalanceMode(whiteBalanceModes);
            smartARController.cameraDevice_.SetWhiteBalanceMode(whiteBalanceModes [whiteBalanceModeSelected_]);
        }

        // Exposure mode
        exposureModeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_EXPOSURE_MODE, -1);
        if (exposureModeSelected_ != -1)
        {
            smartar.ExposureMode[] exposureModes = new smartar.ExposureMode[ExposureModeArray.Length];
            smartARController.cameraDevice_.GetSupportedExposureMode(exposureModes);
            smartARController.cameraDevice_.SetExposureMode(exposureModes [exposureModeSelected_]);
        }

        // Still image size
        stillImageSizeSelected_ = PlayerPrefs.GetInt(PREFERENCE_KEY_STILL_IMAGE_SIZE, -1);
        smartar.Size[] sizes2 = new smartar.Size[SUPPORTED_NUM_OF_IMAGE_SIZE_MAX];
        int numOfStillImageSize = smartARController.cameraDevice_.GetSupportedStillImageSize(sizes2);
        int selectedNum = stillImageSizeSelected_ == -1 ? getBiggestStillImageSize(sizes2, numOfStillImageSize) : stillImageSizeSelected_;
        smartARController.cameraDevice_.SetStillImageSize(sizes2 [selectedNum].width_, sizes2 [selectedNum].height_);

        // Showw status
        showStatus_ = PlayerPrefs.GetInt(PREFERENCE_KEY_SHOW_STATUS, 1) != 0;

        // Sensor device
        useSensorDevice_ = PlayerPrefs.GetInt(PREFERENCE_KEY_SENSOR_DEVICE, 1) != 0;
        if (useSensorDevice_)
        {
            smartARController.sensorDevice_.Start();
        }
        else
        {
            smartARController.sensorDevice_.Stop();
        }

        // Remove lost landmarks
        removeLostLandmarks_ = PlayerPrefs.GetInt(PREFERENCE_KEY_REMOVE_LOST_LANDMARKS, 0) != 0;

        // Triagulate Masks
        useTriangulateMasks_ = PlayerPrefs.GetInt(PREFERENCE_KEY_TRIANGULATE_MASK, 0) != 0;

        // Dense map mode
        denseMapMode_ = smartARController.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING
                        ? (smartar.DenseMapMode)PlayerPrefs.GetInt(PREFERENCE_KEY_DENSE_MAP_MODE, 0)
                        : smartar.DenseMapMode.DENSE_MAP_DISABLE;
        smartARController.recognizer_.SetDenseMapMode(denseMapMode_);
        smartARController.recognizerSettings_.denseMapMode = denseMapMode_;

        // Search policy
        searchPolicy_ = smartARController.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING
                        ? (smartar.SearchPolicy)PlayerPrefs.GetInt(PREFERENCE_KEY_SEARCH_POLICY, (int) smartARController.recognizerSettings_.searchPolicy)
                        : smartar.SearchPolicy.SEARCH_POLICY_FAST;
        smartARController.recognizer_.SetSearchPolicy(searchPolicy_);
    }

    private void savePreferences()
    {
        PlayerPrefs.SetInt(PREFERENCE_KEY_FOCUS_MODE, focusModeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_FLASH_MODE, flashModeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_FPS_RANGE, videoImageFpsRangeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_SCENE_MODE, sceneModeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_WHITE_BALANCE_MODE, whiteBalanceModeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_EXPOSURE_MODE, exposureModeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_STILL_IMAGE_SIZE, stillImageSizeSelected_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_SENSOR_DEVICE, useSensorDevice_ ? 1 : 0);
        PlayerPrefs.SetInt(PREFERENCE_KEY_SHOW_STATUS, showStatus_ ? 1 : 0);
        PlayerPrefs.SetInt(PREFERENCE_KEY_REMOVE_LOST_LANDMARKS, removeLostLandmarks_ ? 1 : 0);
        PlayerPrefs.SetInt(PREFERENCE_KEY_TRIANGULATE_MASK, useTriangulateMasks_ ? 1 : 0);
        PlayerPrefs.SetInt(PREFERENCE_KEY_DENSE_MAP_MODE, (int)denseMapMode_);
        PlayerPrefs.SetInt(PREFERENCE_KEY_SEARCH_POLICY, (int)searchPolicy_);

        PlayerPrefs.Save();
    }

    private void resetAllSettings()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        focusModeSelected_ = -1;
        flashModeSelected_ = -1;
        videoImageFpsRangeSelected_ = -1;
        sceneModeSelected_ = -1;
        whiteBalanceModeSelected_ = -1;
        exposureModeSelected_ = -1;
        stillImageSizeSelected_ = -1;
        useSensorDevice_ = true;
        showStatus_ = true;
        removeLostLandmarks_ = false;
        useTriangulateMasks_ = false;

        denseMapMode_ = smartar.DenseMapMode.DENSE_MAP_DISABLE;
        smartARController.recognizer_.SetDenseMapMode(denseMapMode_);
        smartARController.recognizerSettings_.denseMapMode = denseMapMode_;

        searchPolicy_ = smartARController.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING
            ? smartARController.recognizerSettings_.searchPolicy
            : smartar.SearchPolicy.SEARCH_POLICY_FAST;

        smartARController.recognizer_.SetSearchPolicy(searchPolicy_);

        smartARController.restartController();
    }

    private IEnumerator waitUntilFinishedWriting(Action callback)
    {
        while (!File.Exists(m_CaptureImagePath))
        {
            yield return null;
        }
        callback();
        yield break;
    }

    private string createCaptureImagePath(string name)
    {
        string path;
        switch(Application.platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
                path = string.Format("{0}/{1}", Application.persistentDataPath, name);
                break;
            case RuntimePlatform.WindowsPlayer:
                path = string.Format("{0}/{1}", Application.dataPath, name);
                break;
            default:
                path = name;
                break;
        }

        return path;
    }

    void onPreCaptureImage()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        _PlaySystemShutterSound();
#elif UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaObject utils = new AndroidJavaObject("com.sony.smartar.utils.CaptureImageUtils"))
        {
            utils.CallStatic("playSystemShutterSound");
        }
#endif
    }

    void onPostCaptureImage()
    {
        m_ShowGUI = true;
        m_DoCapture = false;
#if UNITY_IPHONE && !UNITY_EDITOR
        _WriteImageToAlbum(m_CaptureImagePath);
#elif UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaObject utils = new AndroidJavaObject("com.sony.smartar.utils.CaptureImageUtils"))
        {
            var scanFilePath = utils.CallStatic<string>("moveToExternalDir", m_CaptureImagePath);
            if (string.IsNullOrEmpty(scanFilePath)) { return; }
            utils.CallStatic("scanCaptureImage", scanFilePath);
        }
#endif
    }

    void OnDestroy()
    {
        // Free fields for LandmarkDrawer
        Marshal.FreeCoTaskMem(landmarkBuffer_);
        landmarkBuffer_ = IntPtr.Zero;
        Marshal.FreeCoTaskMem(initPointBuffer_);
        initPointBuffer_ = IntPtr.Zero;
    }

#if UNITY_IPHONE && !UNITY_EDITOR
    void onPostWriteImageToAlbums(string errorDescription)
    {
        if (string.IsNullOrEmpty(errorDescription))
        {
            File.Delete(m_CaptureImagePath);
        }
    }
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void _PlaySystemShutterSound();
    [DllImport("__Internal")]
    private static extern void _WriteImageToAlbum(string path);
#endif
}
