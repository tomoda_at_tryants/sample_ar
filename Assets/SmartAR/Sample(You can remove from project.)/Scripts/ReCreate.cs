﻿using UnityEngine;
using System.Collections;

public class ReCreate : MonoBehaviour
{
    public SmartARController smartARController;
    public GUISkin mySkin;

    void OnGUI()
    {
        if (smartARController.smart_.isConstructorFailed()) { return; }

        GUI.skin = mySkin;

        var btnWidth = 500f;
        var btnHeight = 100f;
        var btnMarginTop = 25f;

        if (GUI.Button(new Rect(Screen.width - btnWidth, 0, btnWidth, btnHeight), "ReCreate Target Tracking"))
        {
            smartARController.reCreateController(smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING);
        }

        if (GUI.Button(new Rect(Screen.width - btnWidth, btnHeight + btnMarginTop, btnWidth, btnHeight), "ReCreate Scene Mapping"))
        {
            smartARController.reCreateController(smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING);
        }
    }
}
