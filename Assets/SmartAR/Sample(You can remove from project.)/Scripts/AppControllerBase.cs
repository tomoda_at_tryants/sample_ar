﻿using UnityEngine;
using System.Collections;

public class AppControllerBase : MonoBehaviour
{
    [SerializeField]
    protected GUISkin mySkin;
    [SerializeField]
    protected SmartARController smartARController;

    protected SmartAREffector[] effectors_ = { };

    protected virtual void Awake()
    {
        effectors_ = FindObjectsOfType<SmartAREffector>();

        mySkin.label.fontSize = 30;
        mySkin.label.alignment = TextAnchor.UpperLeft;
        mySkin.label.normal.textColor = Color.green;
    }

    protected virtual void Update()
    {
        // Escape key is application quit.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
            return;
        }

        if (smartARController.smart_.isConstructorFailed()) { return; }
        if (smartARController.targets_ == null) { return; }

        // For Frame
        foreach (var target in smartARController.targets_)
        {
            foreach (var effector in effectors_)
            {
                if (effector.result_.isRecognized_)
                {
                    if (effector.targetID == null)
                    {
                        var child = effector.transform.FindChild("Frame");
                        if (child == null) { return; }
                        child.localScale = new Vector3(300f, 150f, 300f);
                    }
                    else if (effector.result_.target_ == target.self_ && !smartARController.isLoadSceneMap_)
                    {
                        var size = new smartar.Vector2();
                        target.GetPhysicalSize(out size);
                        var child = effector.transform.FindChild("Frame");
                        var nearClipPlane = Camera.main.nearClipPlane * 1000;
                        if (child == null) { return; }
                        child.localScale = new Vector3(size.x_ * nearClipPlane, 150f, size.y_ * nearClipPlane);
                    }
                }
            }
        }
    }

    protected virtual void OnGUI()
    {
        GUI.skin = mySkin;

        smartCheckError();
    }

    private void smartCheckError()
    {
        if (!smartARController.smart_.isConstructorFailed()) { return; }

        mySkin.label.fontSize = Screen.width / 20;
        mySkin.label.alignment = TextAnchor.MiddleCenter;
        mySkin.label.normal.textColor = Color.red;

        var message = "";
        switch (smartARController.smart_.getInitResultCode())
        {
            case smartar.Error.ERROR_EXPIRED_LICENSE:
                message = "SmartAR SDK expired license.";
                break;
            default:
                message = "Smart initialized error.";
                break;
        }

        message += "\nPlease exit the application.";
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height), message);
    }
}
