#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>

extern "C" void _WriteImageToAlbum(const char* path)
{
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:path]];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    NSMutableDictionary *metadata = [[NSMutableDictionary alloc] init];
    [library writeImageToSavedPhotosAlbum:image.CGImage metadata:metadata completionBlock:^(NSURL *assetURL, NSError *error) {
        NSString *errorDescription = error == nil ? @"" : error.description;
        UnitySendMessage("SmartAR Camera", "onPostWriteImageToAlbums", [errorDescription UTF8String]);
    }];
}

extern "C" void _PlaySystemShutterSound()
{
    AudioServicesPlaySystemSound(1108);
}
